{
  description = "Flake for Olefin python module developed by Gianluca Rigolleti at CERN";

  outputs =
    { self, nixpkgs }:
    let
      system = "x86_64-linux";
    in
    {
      devShells.${system}.default =
        with import nixpkgs {
          inherit system;
          config.allowUnfree = true;
        };
        let
          python-development = python312.withPackages (
            p: with p; [
              matplotlib
              pandas
              numpy
              black
              python-box
              numbaWithCuda
              scipy
              pyyaml
              psycopg2
              tqdm
              pytest
              tox
            ]
          );

          python-venv = python312.withPackages (p: with p; [ virtualenv ]);

          buildInputs = [
            python-venv
            zlib
          ];
        in
        mkShell {
          packages = buildInputs;
          shellHook = ''
            export LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
            export LD_LIBRARY_PATH="${pkgs.stdenv.cc.cc.lib.outPath}/lib:$LD_LIBRARY_PATH"
          '';
        };
    };
}
