import pandas as pd
from pathlib import Path
import olefin
import pytest
from box import Box


def test_get_real_run_path(data_path, run_path, hv_path):
    with pytest.raises(ValueError):
        test_wrong_run_path = Path(data_path)
        olefin.RunAnalysis.get_real_run_path(test_wrong_run_path)
    test_real_run_path = Path(run_path)
    real_run_path = olefin.RunAnalysis.get_real_run_path(test_real_run_path)
    assert run_path == real_run_path
    test_sub_run_path = Path(run_path / "MIX0")
    real_run_path = olefin.RunAnalysis.get_real_run_path(test_sub_run_path)
    assert run_path == real_run_path
    test_sub_run_path = Path(hv_path)
    real_run_path = olefin.RunAnalysis.get_real_run_path(test_sub_run_path)
    assert run_path == real_run_path


def test_config_from_path(data_path, run_path, hv_path):
    with pytest.raises(ValueError):
        olefin.RunAnalysis.config_from_path(run_path / "olefin_config.txt")
    config = olefin.RunAnalysis.config_from_path(run_path / "olefin_config.yaml")
    assert isinstance(config, Box)
    assert hasattr(config, "config_path")


def test_config_from_folder(data_path, run_path, hv_path):
    with pytest.raises(FileNotFoundError):
        olefin.RunAnalysis.config_from_folder(run_path, pattern="**/wrong_name*")
    config = olefin.RunAnalysis.config_from_folder(run_path)
    assert isinstance(config, Box)
    assert hasattr(config, "config_path")


def test_factory(data_path, run_path, run_analysis):
    assert "path" in run_analysis.config.run
    assert isinstance(run_analysis, olefin.RunAnalysis)
    assert hasattr(run_analysis, "indata")
    assert isinstance(run_analysis.indata, pd.DataFrame)
    assert hasattr(run_analysis, "path")
    assert isinstance(run_analysis.path, Path)
    assert not run_analysis.indata.empty
    assert "correction_factor" in run_analysis.config.run
    assert run_analysis.config.run.correction_factor != 0
    assert "run_name" in run_analysis.indata.index.names
    assert "voltage_app" in run_analysis.indata.index.names
    assert "voltage" in run_analysis.indata.index.names
    assert "rpc" in run_analysis.indata.index.names


def test_from_saved(data_path, run_path, hv_path, tmp_path):
    analysis = olefin.EPDTRunAnalysis.from_path(run_path, keep_data=True)
    analysis.to_pickle(tmp_path / "olefin_analysis.pkl")
    analysis = olefin.EPDTRunAnalysis.from_saved(tmp_path / "olefin_analysis.pkl")
    assert isinstance(analysis, olefin.RunAnalysis)
    assert hasattr(analysis, "event_data") and not analysis.event_data.empty
    assert hasattr(analysis, "signal_data") and not analysis.signal_data.empty


@pytest.mark.parametrize("parallel", [False, True])
def test_parallel(data_path, run_path, hv_path, parallel):
    run_analysis = olefin.EPDTRunAnalysis.from_path(run_path, parallel=parallel)
    assert isinstance(run_analysis, olefin.RunAnalysis)
    assert hasattr(run_analysis, "indata")
    assert isinstance(run_analysis.indata, pd.DataFrame)
    assert not run_analysis.indata.empty


def test_fit_efficiency(run_analysis):
    data = run_analysis.indata.reset_index()
    grouped = data.groupby("voltage")
    hv = grouped.voltage.mean()
    eff = grouped.efficiency.mean()
    res = run_analysis.fit_efficiency(hv, eff)
    assert isinstance(res, dict)
    assert set(res.keys()) == {
        "effmax",
        "effmax_err",
        "gamma",
        "gamma_err",
        "hv50",
        "hv50_err",
        "chi2",
    }


def test_missing_environmental_parameter_in_configs(run_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    config.run.p = None
    config.run.t = None
    run_analysis = olefin.EPDTRunAnalysis.from_path(run_path, config, parallel=True)
    run_analysis.run()


def test_fit(run_path, run_analysis):
    features = {
        "effmax",
        "effmax_err",
        "gamma",
        "gamma_err",
        "hv50",
        "hv50_err",
        "chi2",
        "knee",
        "working_point",
        "working_point_error",
        "avalanche",
        "avalanche_error",
        "streamer",
        "streamer_error",
        "streamer_probability",
        "streamer_probability_error",
        "streamer_variability_lower",
        "streamer_variability_upper",
        "streamer_variability",
        "avalanche_streamer_separation",
        "prompt_charge",
        "prompt_charge_error",
        "time_res",
        "time_res_error",
        "cluster_size",
        "cluster_size_error",
        "cluster_size_avalanche",
        "cluster_size_avalanche_error",
        "cluster_size_streamer",
        "cluster_size_streamer_error",
        "rate",
        "rate_error",
        "currents_standard_mean",
        "currents_standard_std",
        "currents_beam_mean",
        "height",
        "height_error",
        "time_over_threshold",
        "time_over_threshold_error",
    }
    assert run_analysis.data is not None and not run_analysis.data.empty
    assert set(run_analysis.data.columns) == features


def test_keep_data(run_analysis):
    assert "voltage" in run_analysis.indata.index.names
    assert "voltage" in run_analysis.acquisition_data.index.names
    assert "voltage" in run_analysis.event_data.index.names
    assert "voltage" in run_analysis.signal_data.index.names


@pytest.mark.skip(reason="To think about what to test")
def test_parse_parameter_file(data_path, run_path, hv_path):
    pass


def test_serialization(tmp_path, run_path, run_analysis):
    savepath = run_analysis.to_pickle()
    custom_savepath = run_analysis.to_pickle(tmp_path / "custom.pkl")
    assert savepath.exists()
    assert custom_savepath.exists()
    custom_savepath = run_analysis.to_excel(tmp_path / "custom.xlsx")
    assert custom_savepath.exists()
    custom_savepath = run_analysis.to_json(tmp_path / "custom.json")
    assert custom_savepath.exists()


@pytest.mark.skip(reason="TODO")
def test_custom_classes(data_path, run_path, hv_path):
    from olefin import SignalAnalysis, EventAnalysis, AcquisitionAnalysis

    analysis = olefin.EPDTRunAnalysis.from_path(
        run_path,
        keep_data=False,
        acquisition_class=AcquisitionAnalysis,
        event_class=EventAnalysis,
        signal_class=SignalAnalysis,
    )

    analysis.run()


def test_currents_file(data_path, run_path, run_analysis):
    olefin.EPDTRunAnalysis.config_from_path(run_path / "olefin_config.yaml")
    assert "currents_standard_mean" in run_analysis.indata.columns
    assert "currents_beam_mean" in run_analysis.indata.columns
    assert "currents_standard_std" in run_analysis.indata.columns


def test_currents_file_non_standard_name(data_path, run_path_non_standard_RPC_name):
    dfcurrents = olefin.analysis.utils.parse_currents_autogen(
        run_path_non_standard_RPC_name / "currents.xlsx"
    )
    ra = olefin.EPDTRunAnalysis.from_path(run_path_non_standard_RPC_name)
    ra.run()
    assert (
        "currents_standard_mean" in ra.indata.columns
        and not pd.isna(ra.indata.currents_standard_mean).all()
    )
    assert (
        "currents_beam_mean" in ra.indata.columns
        and not pd.isna(ra.indata.currents_beam_mean).all()
    )
    assert (
        "currents_standard_std" in ra.indata.columns
        and not pd.isna(ra.indata.currents_standard_std).all()
    )


def test_parameter_config_from_manual_missing_values(data_path, run_path, hv_path):
    # Test if parsing from manual configuration works
    parameter_map = {0: 9400, 1: 9600, 2: 9800, 3: 10000, 4: 10200, 5: 10400}
    run_analysis = olefin.EPDTRunAnalysis.from_path(run_path)
    run_analysis.config.run.parameter_map = parameter_map
    assert run_analysis.config.run.parameter_map == parameter_map
    run_analysis.run()
    assert (
        run_analysis.indata.index.get_level_values("voltage_app")
        .isin(list(parameter_map.values()))
        .all()
    )
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    config.run.parameter_map = parameter_map
    run_analysis = olefin.EPDTRunAnalysis.from_path(run_path)


def test_parameter_config_from_file(data_path, run_path, run_analysis):
    # This time from file
    parameter_file = run_path / "parameters.dat"
    parameter_map = olefin.EPDTRunAnalysis.parse_parameter_file(
        parameter_file, column_map={1: "folder_num", 2: "voltage"}
    )
    assert run_analysis.config.run.parameter_map == parameter_map


def test_epdt_run_analysis(data_path, run_path, run_analysis):
    assert hasattr(run_analysis, "indata")
    assert hasattr(run_analysis, "data")
    assert hasattr(run_analysis, "signal_data")
    assert hasattr(run_analysis, "event_data")
    assert hasattr(run_analysis, "acquisition_data")
    assert isinstance(run_analysis.indata, pd.DataFrame)
    assert isinstance(run_analysis.data, pd.DataFrame)
    assert isinstance(run_analysis.signal_data, pd.DataFrame)
    assert isinstance(run_analysis.event_data, pd.DataFrame)
    assert isinstance(run_analysis.acquisition_data, pd.DataFrame)


def test_analysis_has_currents_data(data_path, run_path, run_analysis):
    assert hasattr(run_analysis, "currents_data")
    assert isinstance(run_analysis.currents_data, pd.DataFrame)
    assert not run_analysis.currents_data.empty


def test_gwp(data_path, run_path, run_analysis):
    assert hasattr(run_analysis, "mixture_gwp")
    assert run_analysis.mixture_gwp != 0


def test_get_signals(run_analysis):
    res = run_analysis.get_signals(run_analysis.signal_data.sample(3))
    assert isinstance(res, pd.DataFrame) and not res.empty
    res = run_analysis.get_signals(run_analysis.event_data.sample(3))
    assert isinstance(res, pd.DataFrame) and not res.empty
    # Test from utils
    subset = run_analysis.signal_data.sample(3)
    res = olefin.analysis.utils.get_signals(
        subset, run_analysis.config, run_analysis.path, repeating_variable="nevent"
    )
    assert isinstance(res, pd.DataFrame) and not res.empty


def test_rate_only_folder(data_path):
    run_path = data_path / "subset_50_events_missing_MIX0"
    run_analysis = olefin.EPDTRunAnalysis.from_path(run_path)
    run_analysis.run()


def test_non_standard_chamber_name(data_path):
    run_path = data_path / "subset_50_events_non_standard_chamber_name"
    run_analysis = olefin.EPDTRunAnalysis.from_path(run_path)
    run_analysis.run()


if __name__ == "__main__":

    run_path = Path(
        "/home/gianluca/ghg-studies/BackupRPC/ep-dt-gas-rpc-2/20210714/no-header-512/ABS213(46)BeamOn/"
    )
    path = olefin.EPDTRunAnalysis.config_from_path(run_path / "olefin_config.yaml")
    run_analysis = olefin.EPDTRunAnalysis.from_path(
        run_path, keep_data=True, parallel=True
    )
    run_analysis.run()
