import olefin
from pathlib import Path
import pytest


def test_signal_analysis(run_path):
    wavefile = run_path / "Rate" / "9800" / "wave5.txt"
    config = olefin.config
    config.rate.record_length = 26000
    _ = olefin.RateSignalAnalysis.from_wavefile(config, wavefile)
    assert True


def test_acquisition_analysis(run_path):
    folder = run_path / "Rate" / "9800"
    config = olefin.EPDTRunAnalysis.config_from_path(run_path / "olefin_config.yaml")
    config.rate.height = 2  # in mV
    # minimum distance used in scipy find_peaks
    config.rate.distance = 30  # in samples
    # window length used in scipy savgol_filter
    config.rate.window_length = 21  # odd number
    # poly order to smooth the signal
    config.rate.polyorder = 5
    # Area of the single strip (single wavefile)
    config.rate.area = 2.4 * 100  # in cm
    # Record length for multi event acquisition
    config.rate.record_length = 2600  # in samples
    config.rate.prominence = [2, None]
    # Folder where rate data is stored from within the run folder
    config.rate.folder = "Rate"
    config.rate.window_size = 0.1
    config.rate.n_windows = 30

    rate_acquisition_analysis = olefin.RateAcquisitionAnalysis.from_folder(
        config, folder
    )
    rate_acquisition_analysis.run()
    config.acquisition.strip_map.pop("wave6")
    rate_acquisition_analysis = olefin.RateAcquisitionAnalysis.from_folder(
        config, folder
    )
    rate_acquisition_analysis.run()


@pytest.mark.parametrize("parallel,keep_data", [[False, True], [False, True]])
def test_run_rate_analysis(run_path, parallel, keep_data):
    run_analysis = olefin.RateRunAnalysis.from_path(
        run_path,
        keep_data=keep_data,
        parallel=parallel,
    )
    repr(run_analysis)


def test_rate_analysis_missing_folder(data_path):
    run_path = data_path / "subset_50_events_missing_rate"
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    config.rate.folder = "Rate"
    with pytest.raises(FileNotFoundError):
        run_analysis = olefin.RateRunAnalysis.from_path(run_path, config)
        run_analysis.run()


def test_to_pickle(run_path, tmp_path):
    rate_run_analysis = olefin.RateRunAnalysis.from_path(run_path, keep_data=True)
    rate_run_analysis.to_pickle()
    rate_run_analysis.to_pickle(tmp_path / "olefin_rate_analysis.pkl")
    assert (tmp_path / "olefin_rate_analysis.pkl").exists()
    assert (run_path / "olefin_rate_analysis.pkl").exists()
    from_saved = olefin.RateRunAnalysis.from_saved(tmp_path)
    assert isinstance(from_saved, olefin.RateRunAnalysis)
    from_saved = olefin.RateRunAnalysis.from_saved(
        tmp_path / "olefin_rate_analysis.pkl"
    )
    assert isinstance(from_saved, olefin.RateRunAnalysis)


def test_rate_analysis_missing_parameter_file(data_path):
    run_path = data_path / "subset_50_events_missing_MIX0"
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    _ = olefin.RateRunAnalysis.from_path(run_path, config)


if __name__ == "__main__":
    data_path = Path(__file__).parent / "data"
    run_folder = data_path / "no-header-512" / "subset_50_events_complete_structure"
    config = olefin.EPDTRunAnalysis.config_from_folder(run_folder)
    print(config.to_dict())
    rate_analysis = olefin.EPDTRunAnalysis.from_path(run_folder, config)
    print(rate_analysis.indata)
    config.rate.prominence = [None, 2]
    rate_analysis = olefin.EPDTRunAnalysis.from_path(run_folder, config)
    print(rate_analysis.indata)
    config.rate.prominence = [2, None]
    rate_analysis = olefin.EPDTRunAnalysis.from_path(run_folder, config)
    print(rate_analysis.indata)
    # rate_acquisition_analysis = olefin.RateAcquisitionAnalysis.from_folder(config, run_folder / 'Rate' / '10400')
