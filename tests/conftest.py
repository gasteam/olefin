import pytest
from pathlib import Path
import olefin


@pytest.fixture(scope="session")
def data_path():
    path = Path(__file__).parent / "data" / "no-header-512"
    assert path.is_dir() and path.exists()
    return path


@pytest.fixture(scope="session")
def run_path(data_path):
    path = data_path / "subset_50_events_complete_structure"
    assert path.is_dir() and path.exists()
    return path


@pytest.fixture(scope="session")
def run_path_non_standard_RPC_name(data_path):
    path = data_path / "subset_50_events_complete_structure_non_standard_RPC_name"
    assert path.is_dir() and path.exists()
    return path


@pytest.fixture(scope="session")
def hv_path(run_path):
    path = run_path / "MIX0" / "HV6"
    assert path.is_dir() and path.exists()
    return path


@pytest.fixture(scope="session")
def run_analysis(run_path):
    run_analysis = olefin.EPDTRunAnalysis.from_path(run_path, keep_data=True)
    run_analysis.run()
    return run_analysis
