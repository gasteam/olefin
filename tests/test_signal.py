# Sample Test passing with nose and pytest
import numpy as np
import pandas as pd
import pytest
import olefin


record_length = 520
n_events = 50
raw_values_shape = (n_events, record_length)


@pytest.fixture(scope="session")
def wavefile(hv_path):
    wavefile = hv_path / "wave6.txt"
    assert wavefile.exists()
    return wavefile


@pytest.fixture(scope="session")
def raw_data(wavefile):
    axis = olefin.config.signal.axis
    raw_values = olefin.read_wavefile(wavefile, record_length=record_length, axis=axis)
    return raw_values


def test_read_wavefile(wavefile):
    axis = olefin.config.signal.axis
    assert wavefile.exists()
    raw_values = olefin.read_wavefile(
        wavefile=wavefile, record_length=record_length, axis=axis
    )
    assert isinstance(raw_values, np.ndarray)
    assert raw_values.shape == raw_values_shape


def test_from_wavefile(wavefile):
    config = olefin.config
    axis = config.signal.axis
    assert isinstance(axis, int)
    signal_analysis = olefin.from_wavefile(
        config=config, wavefile=wavefile, record_length=record_length, axis=axis
    )
    assert isinstance(signal_analysis, olefin.SignalAnalysis)
    assert hasattr(signal_analysis, "indata")
    assert signal_analysis.indata.shape == raw_values_shape


def test_signal_analysis_class(raw_data):
    config = olefin.config
    signal_analysis = olefin.SignalAnalysis(config, raw_data)
    np.testing.assert_array_equal(signal_analysis.indata, raw_data)


def test_signal_analysis_run(wavefile):
    config = olefin.config
    axis = config.signal.axis
    assert isinstance(axis, int)
    features = [
        "baseline_mean",
        "charge",
        "baseline_rms",
        "time_peak",
        "tot_start",
        "tot_stop",
        "tot",
        "height",
        "inverse_height",
        "fired",
        "ratio",
        "signal_type",
    ]

    signal_analysis = olefin.from_wavefile(config, wavefile, record_length, axis)
    signal_analysis.run()

    assert isinstance(signal_analysis.data, pd.DataFrame)
    assert set(signal_analysis.data.columns) == set(features)
    assert signal_analysis.data.shape[1] == len(features)
    assert signal_analysis.data.shape[0] == raw_values_shape[0]


@pytest.mark.parametrize("events", [[1], [1, 2, 3], "all"])
def test_get_events(wavefile, events):
    config = olefin.config
    axis = config.signal.axis
    signal_analysis = olefin.from_wavefile(config, wavefile, record_length, axis)
    res = signal_analysis.get_events(events)
    assert isinstance(res, np.ndarray)
    assert len(res)
    if not isinstance(events, str):
        assert res.shape == (len(events), record_length)


@pytest.mark.parametrize("positive_polarity", [False, True])
def test_signal_polarity(raw_data, positive_polarity):
    config = olefin.config
    # Positive polarity
    config.signal.positive_pulse_polarity = positive_polarity
    signal_analysis = olefin.SignalAnalysis(config, raw_data)
    epdt_signal_analysis = olefin.EPDTSignalAnalysis(config, raw_data)
    signal_analysis.calc_features(signal_analysis.indata)
    epdt_signal_analysis.calc_features(epdt_signal_analysis.indata)
