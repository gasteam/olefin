import olefin
import logging
from olefin.analysis.implementations.signal import EPDTSignalAnalysis


def test_epdt_signal_analysis(run_path):
    config = olefin.config
    wavefile = olefin.read_wavefile(
        run_path / "MIX0" / "HV5" / "wave6.txt", record_length=520
    )

    default_signal_analysis = EPDTSignalAnalysis(config, wavefile)
    default_signal_analysis.run()
    assert hasattr(default_signal_analysis, "data")
    logging.info(default_signal_analysis.data)
