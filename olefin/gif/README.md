# gif.py

The gif.py module is intendend to help analyse data from current scans of RPC. Although
the data structure of a HV scan is simpler than the one of efficiency scan, the module
provides two classes, `GIFHelper` and `HVScan`, to help aggregate different data sources together.

## GIFHelper

The `GIFHelper` class is intended to be used to analyse a folder of data of a particular folder.
A folder can contain 1 or more runs in the form of `scan_*.csv` files.

The class provides different methods to retrieve data from different sources, like Excel checklist or
Influxdb dip data. A list of required attributes with their default values is shown here:

```python
# Maps for channels => RPC
helper.rpc_map = {
    0: {'name':'RPC0', 'width': 80, 'height': 100},
    1: {'name':'RPC1', 'width': 80, 'height': 100}
}
# Base folder of data
helper.basepath = Path('/eos/project/g/ghg-studies/BackupRPC/gif/')
# Excel checklist path
helper.checklistpath = Path('/eos/project/g/ghg-studies/BackupRPC/GIFpp checklist.xlsx')
# Credentials to connect to InfluxDB
helper.client = InfluxDBClient(host='dbod-epdtmon.cern.ch', port=8081, username='admin', password='RPCgem2018', ssl=True, verify_ssl=False, database='data')
# Reference pressure and temperature
helper.p0, helper.t0 = 965, 293.15
# Reference start time for integrated charge calculation
helper.tstart = '2020-08-04'
# Delta time over which get mean value of the pressure and temperature
helper.delta = '1h'
# Parameters for the precision of the InfluxDB data retrieved
helper.db_params = {'precision':'s'}
# Names for pressure, temperature and humidity
helper.pressure_dip_sub = 'dip/GIFpp/Atmospheric_Pressure'
helper.temperature_dip_sub = 'dip/GIFpp/Temp_Inside_Bunker'
helper.humidity_dip_sub = 'dip/GIFpp/Humidity_Inside_Bunker'
helper.source_attenuation_dip_sub = 'dip/GIFpp/Attenuators/UpStreamPos/EffectiveAttenuation'
helper.source_status_dip_sub = 'dip/GIFpp/Irradiator/SourceON'
# Parameters to be used as reference values for aging performance
helper.physics_voltage = 11000
helper.ohmic_voltage = 4000
# Module name for connecting to the high voltage
helper.hv_module_name = 'hvrpc256new'
# Row in the excel file for the checklist
helper.row_dates = 2
# Column from wchi to extract dates in the checklist
helper.col_dates_start = 6
# Sheet name of the checklist data
helper.sheet_name = 'RPC'
```

### How to analyse data

If the scan is "standard" and the parameters are configured correctly
the `run()` method tries to automatically analyse the folder supplied.

```python
helper = GIFHelper(date='yyyymmdd')
helper.run(verbose=True)
```

An helper object can be inspected by checking the `.data` attribute.
Each helper should have 1 or more `HVScan` objects containing the dataframes
of the raw and aggregated data of the scans. The scans are accessible via
the `.hv_scans` attribute:

```python
helper.hv_scans  # prints a list of dicts of HVScan objects
```

To plot the data the `.plot_scans()` method may be useful:

```python
fig, axs = helper.plot_scans()
```

the method can also be used to plot hv_scans object that are not present
in the helper object:

```python
hv_scan1 = HVScan(filepath1, helper)
hv_scan1 = HVScan(filepath2, helper)
fig, axs = helper.plot_scans([hv_scan1, hv_scan2])
```
