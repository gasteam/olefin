import pandas as pd
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from influxdb import InfluxDBClient
import urllib3

urllib3.disable_warnings()
import pickle
import pprint
import io

plt.style.use(
    "https://gist.githubusercontent.com/themutt/34b9121f44a4a525734c9c7c8577233d/raw/946d7a234cd4bc41792d2c3248300e89074fbe75/rootlike.mplstyle"
)


class HVScan:
    def __init__(self, filepath, helper, *args, **kwargs):
        """This is an utility class to help reading, parsing and add metadata
        to hv scan performed. Each HVScan object refers to a file where the
        core data (HV, currents) are present."""
        self.filepath = filepath
        self.scan_name = self.filepath.stem
        self.helper = helper

        self.temperature = kwargs.get("temperature", 293.15)
        self.pressure = kwargs.get("pressure", 965)
        self.t0 = kwargs.get("t0", 293.15)
        self.p0 = kwargs.get("p0", 965)

        self.df = pd.read_csv(self.filepath)
        self.dfagg = self.aggregate_df(self.df)
        self.timestamp = HVScan.parse_datetime_scan(self.filepath)

        self.source_info = self.get_source_info(self.timestamp, "1h")

        self.physics_voltage = kwargs.get("physics_voltage", 11000)
        self.ohmic_voltage = kwargs.get("ohmic_voltage", 4000)
        self.physics_currents = self.get_currents(self.dfagg, self.physics_voltage)
        self.ohmic_currents = self.get_currents(self.dfagg, self.ohmic_voltage)

    def aggregate_df(self, df):
        """This function aggregates the data in the df by the channel and the vmon rounded
        to the dozens. It returns a dataframe with the columns:
        - channel
        - vmon
        - veff
        - current"""
        df["veff"] = self.temperature / self.t0 * self.p0 / self.pressure * df["vmon"]
        dfagg = (
            df.assign(vmon=lambda x: x.round(decimals=-1))
            .groupby(["channel", "vmon", "veff"])
            .agg({"current": ["mean", "std"]})
            .rename(columns={"mean": "", "std": "error"})
            .reset_index()
        )
        dfagg.columns = [" ".join(col).strip() for col in dfagg.columns.values]
        return dfagg

    @classmethod
    def parse_datetime_scan(cls, filepath):
        """Returns a pd.Timestamp object of the scan given its pathlib.Path
        path.
        :param filepath pathlib.Path: the path of the scan file"""
        date = filepath.parent.stem
        time = "".join(filepath.stem.strip("scan_").split("_"))
        datetime = pd.to_datetime(date + " " + time, format="%Y%m%d %H-%M-%S")
        return datetime

    def get_currents(self, df, voltage_point):
        """Return the value of the current interpolated at
        the voltage supplied. The interpolation is linear.
        Returns a dict channel -> interpolated current.
        :param df pd.DataFrame: The dataframe to extract data from
        :param voltage_point float: the interpolation voltage point"""
        #
        channels = df.channel.unique()
        res = {}
        for channel in channels:
            subset = df.query("channel == @channel")
            veff = subset["veff"]
            current = subset.current
            current_error = subset["current error"]
            interpolated_current = np.interp(voltage_point, veff, current)
            interpolated_current_error = np.interp(voltage_point, veff, current_error)
            res[channel] = {
                "value": interpolated_current,
                "error": interpolated_current_error,
            }
        return res

    def get_source_info(self, timestamp, delta):
        """Get the information about the source of the gif++.
        Given the timestamps and delta, it returns a dict
        contianing the source status (0 off, 1 on), the
        attenuation factor and the effective attenuation (0 means
        off, 1.4, 2.2, 10, 100, etc. means all on with that filter)"""
        # Retrieve source parameters
        source_status = self.helper.get_dip_parameter(
            self.helper.source_attenuation_dip_sub, timestamp, delta
        )
        source_attenuation = self.helper.get_dip_parameter(
            self.helper.source_status_dip_sub, timestamp, delta, field="Int_value"
        )

        return {
            "source_status": source_status,
            "source_attenuation": source_attenuation,
            "source_intensity": 0 if source_status == 0 else source_attenuation,
        }

    def __repr__(self):
        """Represent the object as a sort of dict
        but removing the dataframes as they have too
        many rows"""
        attributes = self.__dict__.copy()
        # Remove df attributes and instead display only their
        # info
        bufdf = io.StringIO()
        self.df.info(buf=bufdf)
        attributes["df"] = bufdf.getvalue()
        bufdfagg = io.StringIO()
        self.dfagg.info(buf=bufdfagg)
        attributes["dfagg"] = bufdfagg.getvalue()
        bufpprint = io.StringIO()
        pprint.pprint(attributes, stream=bufpprint)
        return bufpprint.getvalue()


class GIFHelper:
    def __init__(self, *args, **kwargs):
        # Set default parameters
        # All the parameters here below are "general" parameters,
        # meaning that their values should be the same amongst
        # different runs
        # Maps for channels => RPC
        self.rpc_map = {
            0: {"name": "RPC0", "width": 80, "height": 100},
            1: {"name": "RPC1", "width": 80, "height": 100},
            2: {"name": "RPC2", "width": 70.5, "height": 100.5},
        }
        # Base folder of data
        self.basepath = Path("/eos/project/g/ghg-studies/BackupRPC/gif/")
        # Excel checklist path
        self.checklistpath = Path(
            "/eos/project/g/ghg-studies/BackupRPC/GIFpp checklist.xlsx"
        )
        # Credentials to connect to InfluxDB
        self.client = InfluxDBClient(
            host="dbod-epdtmon.cern.ch",
            port=8081,
            username="admin",
            password="RPCgem2018",
            ssl=True,
            verify_ssl=False,
            database="data",
        )
        # Reference pressure and temperature
        self.p0, self.t0 = 965, 293.15
        # Reference start time for integrated charge calculation
        self.tstart = "2020-08-04"

        # Delta time over which get mean value of the pressure and temperature
        self.delta = "1h"
        # Parameters for the precision of the InfluxDB data retrieved
        self.db_params = {"precision": "s"}
        # Names for pressure, temperature and humidity
        self.pressure_dip_sub = "dip/GIFpp/Atmospheric_Pressure"
        self.temperature_dip_sub = "dip/GIFpp/Temp_Inside_Bunker"
        self.humidity_dip_sub = "dip/GIFpp/Humidity_Inside_Bunker"
        self.source_attenuation_dip_sub = (
            "dip/GIFpp/Attenuators/UpStreamPos/EffectiveAttenuation"
        )
        self.source_status_dip_sub = "dip/GIFpp/Irradiator/SourceON"
        # Parameters to be used as reference values for aging performance
        self.physics_voltage = 11000
        self.ohmic_voltage = 4000
        # Module name for connecting to the high voltage
        self.hv_module_name = "hvrpc256new"
        # Row in the excel file for the checklist
        self.row_dates = 2
        # Column from wchi to extract dates in the checklist
        self.col_dates_start = 6
        # Sheet name of the checklist data
        self.sheet_name = "RPC"
        self.integrated_charge = {}
        # Update provided parameters
        self.__dict__.update(kwargs)

    def get_dip_parameter(self, param_name, tstart, delta, field="Float_value"):
        """return the mean value of an environmental parameter from dip subscription.
        :param param_name str: the environmental parameter. Can be one of 'Atmospheric_Pressure', 'Temp_Inside_Bunker', 'Humidity_Inside_Bunker'
        :param tstart pd.Timestamp: the right side of the time interval of interest
        :param delta str: the time delta to define the left side of the interval. Could be '1h' for example
        """
        tstart = pd.to_datetime(tstart)
        tstart = tstart.isoformat()
        query_str = f"SELECT mean(\"{field}\") FROM \"dip\" WHERE (\"sub\" = '{param_name}') AND time >= '{tstart}Z' - {delta} AND time <= '{tstart}Z'"
        result = self.client.query(query_str, params=self.db_params).get_points()
        parameter = list(result)[0]["mean"]
        return parameter

    def get_integrated_charge(
        self,
        name,
        tend=None,
        width=80,
        height=100,
        module="hvrpc256new",
        tstart="2020-08-04",
    ):
        """Return the integrated charge per cm2 computed as cumulative sum of the current from InfluxDB
        :param tend str: The date until which the integrated charge should be computed
        :param name str: Name of the channel. Usually it's 'RPC0' or 'RPC1'
        :param width int: the width in cm of the chamber
        :param height int: the height of the chamber
        :param module str: the name of the power supply module
        :param tstart str: the initial time of integration in epoch ms or iso format"""
        tstart = pd.to_datetime(tstart)
        tstart = tstart.isoformat()
        if tend:
            tend = pd.to_datetime(tend)
            tend = tend.isoformat()
            end_time_str = f"AND time <= '{tend}Z'"
        else:
            end_time_str = ""
        query = (
            f"""SELECT integral("IMon")  / (1000 * {width} * {height}) FROM "hv" WHERE ("module" = '{module}' AND "channel_name" = '{name}') AND time >= '{tstart}Z' """
            + end_time_str
        )
        result = self.client.query(query, params=self.db_params)
        if len(result):
            points = result.get_points()
            charge = list(points)[0]["integral"]
            return charge

    def get_checklist_data(
        self, filepath, date, row_dates=2, col_dates_start=6, sheet_name="RPC"
    ):
        """This function tries to get the metadata about the closest check done
        with respect to the 'date' variables supplied. Ex. if date='2020-04-10'
        and in the excel file '2020-04-08' and '2020-04-14' exists, it will return
        the data corresponding to the closest previous value, i.e. '2020-04-08'.
        This functions assumes the structure of the checklist file to be done in columns.
        :param filepath str: the path to the excel checklist
        :param date str: the date to search
        :param row_dates int: the row of the dates in the excel file
        :param col_dates_start: the column in the excel file from which the dates should be extracted
        :param sheet_name str: the name of the sheet to work on"""
        from openpyxl import load_workbook

        date = pd.to_datetime(date)
        wb = load_workbook(filepath, read_only=True, data_only=True)
        ws = wb[sheet_name]
        dates = []  # Used for storing dates
        cols = []  # Used for storing column indexes
        # Extract the dates from the excel file
        for row in ws.iter_rows(
            min_row=row_dates, max_row=row_dates, min_col=col_dates_start
        ):
            for cell in row:
                # It might be that empty cells are returned
                if cell.value:
                    try:
                        dates.append(pd.to_datetime(cell.value))
                        cols.append(cell.column)
                    # It might be that dates are xx/xx/2020, not yet compiled
                    except ValueError:
                        continue
        # Use a pandas time series to find the closest date
        dates = pd.Series(cols, index=dates)
        before_dates = dates.truncate(after=pd.to_datetime(date))
        closest_column_idx = before_dates[-1]
        # Get the column data together with its label
        column_keys = ws.iter_rows(min_col=1, max_col=1, min_row=5)
        column_values = ws.iter_rows(
            min_col=closest_column_idx, max_col=closest_column_idx, min_row=5
        )

        col_keys_flat = list(cell.value for row in column_keys for cell in row)
        col_values_flat = list(cell.value for row in column_values for cell in row)

        dict_data = dict(zip(col_keys_flat, col_values_flat))

        return dict_data

    def get_hv_scans(self, folderpath):
        """Analyze the folder path to look for scan_*.csv files.
        It returns a list of HVScan objects."""
        folderpath = Path(folderpath)
        hvscans = []
        for file in folderpath.glob("scan_*.csv"):
            timestamp = HVScan.parse_datetime_scan(file)
            hvscan = HVScan(
                file,
                self,
                temperature=self.get_dip_parameter(
                    self.temperature_dip_sub, timestamp, self.delta
                )
                + 273.15,
                pressure=self.get_dip_parameter(
                    self.pressure_dip_sub, timestamp, self.delta
                ),
                physics_voltage=self.physics_voltage,
                ohmic_voltage=self.ohmic_voltage,
            )
            hvscans.append(hvscan)
        return hvscans

    def plot_scans(
        self, hv_scans=None, subplots=False, figsize=None, dpi=100, labels="scan_name"
    ):
        """Return fig, axs of a scan or a list of scans
        :param hv_scans HVScan or list(HVScan): the object HVScan, single or as a list. If None, it will look for hv_scans in self
        :param subplots bool: wether to print the different scans in different subplots or not
        :param figsize tuple(int, int): the size in inches of the figure. If nothing is passed it will be computed automatically
        :param dpi int: the dots per inch resolution of the plot+
        :param labels list or None: the attribute of the HVScan object to print"""
        if not hv_scans:
            try:
                hv_scans = self.hv_scans
            except:
                print("No hv scans present in this object!")
        if isinstance(hv_scans, HVScan):
            scans = [hv_scans]
        else:
            scans = hv_scans
        num_scans = len(hv_scans)
        rows = 1 if not subplots else num_scans
        max_num_channels = max(
            [len(hv_scan.dfagg.channel.unique()) for hv_scan in hv_scans]
        )
        figsize = figsize or (4 * max_num_channels, 4 * rows)
        fig, axs = plt.subplots(
            nrows=rows, ncols=max_num_channels, figsize=figsize, dpi=dpi
        )
        if rows == 1:
            axs = np.array([axs])
        for ix_scan, hv_scan in enumerate(hv_scans):
            nrow = ix_scan if subplots else 0
            for channel, group in hv_scan.dfagg.groupby("channel"):
                if labels:
                    if isinstance(labels, str):
                        label = hv_scan.__dict__[labels]
                    elif isinstance(labels, list):
                        label = "\n".join(
                            [f"{label} {hv_scan.__dict__[label]}" for label in labels]
                        )
                axs[nrow, channel].plot(group.veff, group.current, ".--", label=label)
                axs[nrow, channel].set_title(self.rpc_map[channel]["name"])
                if labels:
                    axs[nrow, channel].legend()
        return fig, axs

    def write(self, filepath):
        """Write object to disk
        :param filepath str|pathlib.Path: the path and name of the object to write"""
        with open(filepath, "wb+") as f:
            pickle.dump(self, f, pickel.HIGHEST_PROTOCOL)
        return filepath

    def read(self, filepath):
        """Read and object from disk
        :param filepath str|pathlib.Path: the path and name of the object to read"""
        with open(filepath, "r") as f:
            obj = pickle.load(f)
        return obj

    def save(self, filepath=None):
        """Save the object to the disk
        :param file path None|str|pathlib.Path: the path where to store the file. If nothing is provided it will save with automatic naming
        """
        if not filepath:
            try:
                filepath = Path(self.basepath) / self.date / ".pkl"
            except:
                filepath = "helper.pkl"
        filepath = self.write(filepath)
        return filepath

    def load(self, filepath):
        """For now it's just an alias of self.read()"""
        return self.read(filepath)

    def run(self, verbose=False):
        """Very questionable method that should analyse the data in the best
        way possible provided all the metadata it has
        :param verbose bool: Print useful logging while analysing data"""
        # Retrieve environmental parameters
        self.pressure = self.get_dip_parameter(
            self.pressure_dip_sub, self.date, self.delta
        )
        self.temperature = self.get_dip_parameter(
            self.temperature_dip_sub, self.date, self.delta
        )
        self.humidity = self.get_dip_parameter(
            self.humidity_dip_sub, self.date, self.delta
        )

        if verbose:
            print(
                "Retrieved environmental parameters: <object>.pressure, <object>.temperature, <object>.humidity"
            )
        # Retrieve the integrated charge so far
        for k, v in self.rpc_map.items():
            charge = self.get_integrated_charge(
                v["name"],
                self.date,
                v["width"],
                v["height"],
                self.hv_module_name,
                self.tstart,
            )
            self.integrated_charge[k] = charge
        if verbose:
            print("Computed integrated charge: <object>.integrated_charge_<name>")
        # Get checklist column closest to the associated date
        self.checklist_data = self.get_checklist_data(
            self.checklistpath,
            self.date,
            self.row_dates,
            self.col_dates_start,
            self.sheet_name,
        )
        if verbose:
            print("Extracted checklist data: <object>.checklist_data")
        self.hv_scans = self.get_hv_scans(self.basepath / self.date)
        if verbose:
            print("Extracted hv scans data: <object>.hv_scans")

    @property
    def data(self):
        """Alias: you can use <object>.info instead
        of <object>.__dict__ to check all the attributes
        of the class"""
        return self.__dict__
