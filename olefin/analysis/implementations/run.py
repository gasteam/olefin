import olefin
from olefin.analysis.interface import BaseAnalysis
import numpy as np
import pandas as pd
import typing as t
from pathlib import Path
import logging
import yaml
from box import Box
import multiprocessing
from concurrent.futures import ProcessPoolExecutor, as_completed
import scipy.interpolate as si
import pickle
import json
from functools import partial
from tqdm.auto import tqdm
import warnings
import platform

warnings.simplefilter("ignore")


logger = logging.getLogger(__name__)
logging.basicConfig(
    format=(
        "%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)s -"
        " %(message)s"
    ),
)


class RateRunAnalysis:
    """Analysis at the highest (run) level.

    A run is a collection of acquisitions done on one or more RPCs. The class
    expects a raw_data parameter to be in the same shape of the results of a
    AcquisitionAnalysis class. This means raw_data should have a shape
    ((n_folders, n_rpcs), n_features) and the computed dataframe will be in the
    shape (n_rpc, n_features).

    Attributes
    ---------
    data : pd.DataFrame
        the data resulted from the .run() method
    config : olefin.config
        the global configuration object
    """

    data = None
    config = None

    def __init__(self, config: "olefin.config", raw_data):
        self.config = config
        self.indata = raw_data

    @classmethod
    def from_path(
        cls,
        run_path: t.Union[str, Path],
        config=None,
        top_folder="no-header-512",
        config_pattern="**/olefin_config.*",
        parallel=True,
        cpu_count=None,
        keep_data=False,
        rate_signal_class: olefin.RateSignalAnalysis = olefin.RateSignalAnalysis,
        rate_acquisition_class: olefin.RateAcquisitionAnalysis = olefin.RateAcquisitionAnalysis,
    ):
        logger.debug("Creating run from path")
        if config is None:
            config = RunAnalysis.config_from_folder(run_path, pattern=config_pattern)
            logger.debug(f"Parsing config from folder: {config}")
        real_run_path = RunAnalysis.get_real_run_path(run_path, top_folder=top_folder)
        config.run.path = str(real_run_path)
        rate_folder = config.rate.folder
        # Merge the default olefin config with the config found either from
        # file or from passed argument.
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        # Run rate analysis if present
        rate_results = []
        rate_acquisitions = []
        rate_acquisition_folders = [
            acquisition_folder
            for acquisition_folder in real_run_path.glob(f"**/{rate_folder}/*")
            if len(list(acquisition_folder.glob("wave*.txt")))
        ]
        if not len(rate_acquisition_folders):
            raise FileNotFoundError(f"No valid rate folders found in {real_run_path}")
        if parallel:
            with ProcessPoolExecutor(max_workers=cpu_count) as executor:
                jobs = {
                    (
                        executor.submit(
                            olefin.analysis.utils.get_rate_acquisition_analysis,
                            parsed_config,
                            acquisition_folder,
                            rate_acquisition_class,
                            rate_signal_class,
                        )
                    ): acquisition_folder
                    for acquisition_folder in rate_acquisition_folders
                }
                with tqdm(total=len(jobs), desc="Rate folders") as pbar:
                    for job_instance in as_completed(jobs):
                        logger.info(f"Job completed: {jobs[job_instance]}")
                        pbar.update(1)
                        try:
                            rate_acquisition_analysis = job_instance.result()
                            rate_acquisition_analysis_data = (
                                rate_acquisition_analysis.data
                            )
                            rate_results.append(rate_acquisition_analysis_data)
                            if keep_data:
                                rate_acquisitions.append(rate_acquisition_analysis)
                        except Exception as exc:
                            logger.exception(
                                "%r generated an exception: %s" % (job_instance, exc)
                            )
        else:
            logger.info("Using single core for acquisition folder analysis")

            for rate_acquisition_folder in tqdm(
                rate_acquisition_folders, desc="Rate folders"
            ):
                logger.debug(f"Acquisition folder: {rate_acquisition_folder}")
                rate_acquisition_analysis = rate_acquisition_class.from_folder(
                    config=parsed_config,
                    folder=rate_acquisition_folder,
                    signal_class=rate_signal_class,
                )

                rate_acquisition_analysis.run()
                rate_results.append(rate_acquisition_analysis.data)
                if keep_data:
                    rate_acquisitions.append(rate_acquisition_analysis)

        # Concat the results from single acquisition analysis
        rate_acquisition_data = pd.concat(rate_results).reset_index()
        # Sort values by folder num for better readability
        rate_acquisition_data = rate_acquisition_data.sort_values(by=["rpc", "voltage"])
        # Add the run_name columns and set it as index
        run_name = RunAnalysis.get_run_name_from_path(real_run_path)
        rate_acquisition_data["run_name"] = run_name
        # rate_acquisition_data = rate_acquisition_data.set_index("run_name", append=True)

        # Correct data for pressure/temperature if possible
        p0 = parsed_config.run.p0 or 965
        t0 = (parsed_config.run.t0 or 20) + 273.15
        if not parsed_config.run.p or not parsed_config.run.t:
            logger.warning(
                'p and t not set in "run" section of configs. Using default values of'
                " 965 mbar and 20 °C"
            )
        p = parsed_config.run.p or 965
        t = (parsed_config.run.t or 20) + 273.15
        correction_factor = t / t0 * p0 / p
        logging.info(f"Applying correction factor (t, t0, p, p0): {(t, t0, p, p0)}")
        parsed_config.run.correction_factor = correction_factor
        rate_acquisition_data["voltage_app"] = rate_acquisition_data["voltage"].copy()
        rate_acquisition_data["voltage"] = np.round(
            rate_acquisition_data["voltage_app"] * correction_factor, decimals=0
        )

        # Set index on run name, rpc and hv
        rate_acquisition_data = rate_acquisition_data.set_index(
            ["run_name", "rpc", "voltage", "voltage_app"]
        ).sort_index()

        # Create the main class
        run_analysis = cls(config=parsed_config, raw_data=rate_acquisition_data)
        # Add attributes to the class
        run_analysis.path = real_run_path
        run_analysis.keep_data = keep_data
        # If keep data is enabled attach the sub analysis dataframes
        if keep_data:
            run_analysis.rate_acquisitions = rate_acquisitions
            signal_data = pd.concat(
                (acquisition.indata) for acquisition in rate_acquisitions
            )
            run_analysis.signal_data = signal_data

        return run_analysis

    def to_pickle(self, path=None):
        """
        Serialize the object with pickle

        Parameters
        ----------
        path : path or Path optional
            the path where to save the object. If not provided, the object will
            be saved in config.run.path with the name "olefin_rate_analysis.pkl"

        Returns
        -------
        Path
            the path to the saved object
        """
        savepath = path or Path(self.config.run.path) / "olefin_rate_analysis.pkl"
        savepath = Path(savepath)
        with open(savepath, "bw+") as f:
            pickle.dump(self, f)

        return savepath

    @classmethod
    def from_saved(
        cls, path: t.Union[str, Path], pattern: str = "**/*olefin_rate_analysis.pkl"
    ):
        """
        Load an analysis class from a saved pickled one.

        Parameters
        ----------
        path : str or Path
            the path to the saved analysis, If a file the extension should be .pkl
            If a folder, the first .pkl files will be read and loaded

        Returns
        -------
        RunAnalysis
            the loaded analysis class

        Raises
        ------
        FileNotFoundError
            if no .pkl files were found in the provided path
        ValueError
            if the read object is not of type RunAnalysis
        """
        path = Path(path)
        if path.is_dir():
            file_list = list(path.glob(pattern))
            if not len(file_list):
                raise FileNotFoundError(f"No {pattern} files found in {path}")
            file_path = file_list[0]
        else:
            file_path = path

        with open(file_path, "rb") as f:
            analysis = pickle.load(f)
            obj_instance = type(analysis)
        if not isinstance(analysis, cls):
            raise ValueError(
                f"Read object is not instance of {cls} but of type {obj_instance}"
            )

        return analysis

    def __repr__(self):
        return ".indata: \n" + repr(self.indata)


class RunAnalysis(BaseAnalysis):
    """Analysis at the highest (run) level.

    A run is a collection of acquisitions done on one or more RPCs. The class
    expects a raw_data parameter to be in the same shape of the results of a
    AcquisitionAnalysis class. This means raw_data should have a shape
    ((n_folders, n_rpcs), n_features) and the computed dataframe will be in the
    shape (n_rpc, n_features).

    Attributes
    ---------
    data : pd.DataFrame
        the data resulted from the .run() method
    config : olefin.config
        the global configuration object
    """

    data = None
    config = None

    def __init__(self, config: "olefin.config", raw_data):
        self.config = config
        self.indata = raw_data
        if "gas_mixture" in self.config.run:
            # Add .gas_data to the object
            self.mixture_gwp = RunAnalysis.calc_gwp(self.config.run.gas_mixture)

    @staticmethod
    def calc_gwp(
        gas_mixture_data: t.Dict,
        database=olefin.analysis.utils.gwp_gases,
        molecular_weights=olefin.analysis.utils.molecular_weight_gases,
    ):
        """Calc GWP based on gas mixture data.
        It uses a database by default available in olefin.utils to lookup
        for each gas defined in gas_mixture_data.

        gas_mixture_data should be a dict where keys is the name of the
        gas (without dashes) and the values is the percentage composition
        in the mixture in range 0-100 %.

        Parameters
        ----------
        gas_mixture_data: dict
            the dict of the gases of the gas mixture. Keys should refer to
            the name of the gas and values to the percentage of the single
            component in the range (0-100)
        database: dict or box
            a database where the keys are the name of the gases and the values
            are the GWP of the gases

        Returns
        -------
        float
            the gas mixture GWP
        """
        total_gwp = 0
        # Normalize the GWP in weight with respect to
        # 1 ton of 100% of CO2 gas mixture (GWP = 1)
        normalization_factor = 100 * molecular_weights["CO2"] * 1
        for gas, percentage in gas_mixture_data.items():
            gwp = database.get(gas.upper())
            molecular_weight = molecular_weights.get(gas.upper())
            if gwp is None or molecular_weight is None:
                raise ValueError(
                    f"GWP/Molecular weight not found for gas {gas} in supplied"
                    " database."
                )
            total_gwp += percentage * molecular_weight * gwp
        total_gwp = total_gwp / normalization_factor
        return total_gwp

    @staticmethod
    def config_from_path(filepath: t.Union[str, Path]) -> Box:
        """Parse a .yml file into a config object.

        Parameters
        ----------
        filepath:
            the path to the config file

        Raises
        ------
        ValueError
            if the file extension is not .yml or .yaml
        """
        filepath = Path(filepath)
        if filepath.suffix in [".yml", ".yaml"]:
            with open(filepath, "r") as f:
                config = Box(yaml.load(f, Loader=yaml.Loader), default_box=True)
                config.config_path = str(filepath)
            return config
        else:
            raise ValueError(
                f"File extension is not .yml or .yaml but {filepath.suffix}"
            )

    @staticmethod
    def config_from_folder(
        run_path: t.Union[str, Path], pattern: str = "**/olefin_config.*"
    ) -> Box:
        """
        Find a config object from a path.

        It will search on run_path the desired pattern and parse the first
        returned file into a box.Box config object. If nothing is found
        an exception will be throws.
        Only yaml files with extension .yml or .yaml are supported

        Parameters
        ----------
        run_path : str or Path
            the path where to search the config
        pattern : str
            a string pattern used to search the object. Only glob.glob accepted
            patterns are valid.

        Returns
        -------
        Box
            a config object from the parsed file

        Raises
        ------
        FileNotFoundError
            If the file is not found in the path
        ValueError
            if the file extension is not .yml or .yaml

        """
        run_path = Path(run_path)
        res = list(run_path.glob(pattern))
        if len(res):
            file = res[0]
        else:
            raise FileNotFoundError(f"olefin_configs not found in {run_path}")
        return RunAnalysis.config_from_path(file)

    @staticmethod
    def get_real_run_path(
        run_path: t.Union[str, Path], top_folder: str = "no-header-512"
    ) -> Path:
        """
        Return the real path of the run.

        This function is used to check if the provided ``run_path`` is the path
        corresponding to a run folder structure. If it is not it will check if
        the provided ``run_path`` is a subfolder of the real run path and will
        return the correct path.

        If instead the provided path doesn't have the correct structure an
        exception will be raised

        Parameters
        ----------
        run_path : str or Path
            the assumed path of the run
        top_folder : str
            the name of the folder that should be just one level on top of the
            folder of the run

        Returns
        -------
        Path
            the real run path

        Raises
        ------
        FileNotFoundError
            if the ``top_folder`` was not found in the provided ``run_path``
        ValueError
            if the ``run_path`` is not a direct subfolder of the ``top_folder``
        """
        # In this case the most top level is CosmicsX folder
        run_path = Path(run_path)
        num_parts = len(run_path.parts)
        try:
            index_no_header_512 = run_path.parts.index(top_folder)
        except ValueError:
            raise FileNotFoundError(f"{top_folder} not found in {run_path}.")
        if num_parts - index_no_header_512 == 2:
            return run_path
        elif num_parts - index_no_header_512 < 2:
            raise ValueError(
                f"run_path should be a direct subfolder of the '{top_folder}'."
                f" {run_path} was provided instead as a run path"
            )
        # If num_parts - index_no_header_512 > 2 it means we are on a subfolder
        # and we should go directly to the subfolder after no-header-512
        levels = num_parts - index_no_header_512 - 3
        return run_path.parents[levels]

    @staticmethod
    def get_run_name_from_path(run_path: t.Union[str, Path]) -> str:
        """Parse the name of the run from the run path.

        By default the run name is the name of the folder containing all the
        data.
        """
        run_path = Path(run_path)
        return run_path.parts[-1]

    @staticmethod
    def find_parameter_file(
        run_path: t.Union[str, Path], pattern="**/[pP]arameters.dat"
    ) -> Path:
        """
        Search the parameters file used to map folder number to voltage.

        Parameters
        ----------
        run_path : str or Path
            the path where to search the parameters file
        pattern : str
            a glob pattern for parameters file
        """
        run_path = Path(run_path)
        results = list(run_path.glob(pattern))
        if len(results):
            return results[0]

    @staticmethod
    def parse_parameter_file(
        filepath: t.Union[str, Path], column_map: dict = {1: "folder_num", 2: "voltage"}
    ) -> t.Dict:
        """
        Parse the parameter file into a dict folder_num -> voltage

        Parameters
        ----------
        filepath : str or Path
            the path of the parameters file
        column_map : dict
            a dict where the keys are the number of columns and the values
            are the name of the columns

        Returns
        -------
        dict
            a dict where keys are the folder numbers and values are the voltage values
        """
        columns = list(column_map.keys())
        df = (
            pd.read_csv(filepath, sep=r"\s+", header=None)
            .loc[:, columns]
            .rename(columns=column_map)
        )
        return dict(zip(df.folder_num, df.voltage))

    @classmethod
    def from_saved(
        cls, path: t.Union[str, Path], pattern: str = "**/*olefin_analysis.pkl"
    ):
        """
        Load an analysis class from a saved pickled one.

        Parameters
        ----------
        path : str or Path
            the path to the saved analysis, If a file the extension should be .pkl
            If a folder, the first .pkl files will be read and loaded

        Returns
        -------
        RunAnalysis
            the loaded analysis class

        Raises
        ------
        FileNotFoundError
            if no .pkl files were found in the provided path
        ValueError
            if the read object is not of type RunAnalysis
        """
        path = Path(path)
        if path.is_dir():
            file_list = list(path.glob(pattern))
            if not len(file_list):
                raise FileNotFoundError(f"No {pattern} files found in {path}")
            file_path = file_list[0]
        else:
            file_path = path

        with open(file_path, "rb") as f:
            analysis = pickle.load(f)
            obj_instance = type(analysis)
        if not isinstance(analysis, cls):
            raise ValueError(
                f"Read object is not instance of {cls} but of type {obj_instance}"
            )

        return analysis

    @classmethod
    def from_path(
        cls,
        run_path: t.Union[str, Path],
        config=None,
        top_folder="no-header-512",
        config_pattern="**/olefin_config.*",
        parallel=True,
        cpu_count=None,
        keep_data=False,
        parameter_file_pattern="**/[pP]arameters.dat",
        parameter_column_map={1: "folder_num", 2: "voltage"},
        acquisition_class=olefin.AcquisitionAnalysis,
        event_class=olefin.EventAnalysis,
        signal_class=olefin.SignalAnalysis,
        rate_analysis_class=RateRunAnalysis,
    ):
        """
        Create a run analysis object from the given path.

        The path should be the path coresponding to the run folder. This function
        will try in any case to the detected the real run path. Also, it will
        check for the present of HV* named folders and will run an acquisition
        analysis on each of them. The results is then concatenated into a pandas
        DataFrame. The function can run the analysi in parallel mode by running
        each acquisition analysis into a separate process on different cores.

        The function will then try to parse the parameters file to assign
        the folder number to a voltage.

        If ``keep_data`` is set to ``True`` each of the acquisition, event, signal
        analysis .data attribute will be stored respectively .acquisition_data,
        .event_data, .signal_data attributes of the initialized run class.

        Parameters
        ----------
        run_path : str or Path
            the path to the run
        config : olefin.config optional
            the global config object. If not provided the function will try to
            search and parse it from the run_path
        top_folder : str
            the name of the top folder of the run_path folder
        config_patter : str
            a glob string to match the config file in the run_path
        parallel : bool
            use multiprocessing ot run Acquisition analysis on different folders
        cpu_count : int optional
            the number of cpu cores to use if parallel is True. By default the
            number of cores in the machine will be used
        keep_data : bool
            if sets to ``True`` it will save the attributes .acquisition_data,
            .event_data, .signal_data in the run object.
        parameter_file_patter : str
            a glob string to match the parameters file
        parameter_column_map : dict
            a dict where keys are the number of the columns and the values are
            the name of the columns in the parameters file
        acquisition_class : AcquisitionAnalysis
            the class used to perform acquisition analysis
        event_class : EventAnalysis
            the class used to perform event analysis
        signal_class : SignalAnalysis
            the class used to perform signal analysis

        Returns
        -------
        RunAnalysis
            the initialized class with the proper .data attributes and optional
            attributes depending on arguments passed by the function.
        """
        logger.debug("Creating run from path")
        if config is None:
            config = RunAnalysis.config_from_folder(run_path, pattern=config_pattern)
            logger.debug(f"Parsing config from folder: {config}")
        real_run_path = RunAnalysis.get_real_run_path(run_path, top_folder=top_folder)
        config.run.path = str(real_run_path)
        mix_folder = config.run.get("mix_folder", "MIX0")
        # Merge the default olefin config with the config found either from
        # file or from passed argument.
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        # Check for HV -> voltage values if possible by checking either a
        # config.run.parameter_map or parsing
        # the 'parameters.dat' file is present

        # This is from file if exists
        parameter_file = RunAnalysis.find_parameter_file(
            real_run_path, parameter_file_pattern
        )
        # This is from parsed_config
        parameter_config = parsed_config.run.parameter_map
        # parameter map from config takes the precedence over
        # the parameter map from file
        if len(parameter_config):
            logger.info(f"Using parameters from config: {parameter_config}")
            parameter_map = dict(parameter_config)
        elif parameter_file:
            parameter_map = RunAnalysis.parse_parameter_file(
                parameter_file, parameter_column_map
            )
            logger.info(
                f"Using parameters from file: {parameter_file}. Map: {parameter_map}"
            )
        else:
            logger.warning(
                f"Parameters config or file not found in {run_path} or its subfolders."
                " Using empty map"
            )
            parameter_map = {}

        # Save it in the run config as 'parameter_map'
        parsed_config.run.parameter_map = parameter_map
        logger.info(f"Parameter map: {parameter_map}")

        # List all the folders containing wavefiles
        acquisition_folders = [
            acquisition_folder
            for acquisition_folder in real_run_path.glob(f"**/{mix_folder}/HV*")
            if len(list(acquisition_folder.glob("wave*.txt")))
        ]
        # Use  the acquisition folder defined by the map HV->voltage
        selected_acquisition_folders = [
            acquisition_folder
            for acquisition_folder in acquisition_folders
            if int(acquisition_folder.stem.strip("HV"))
            in parsed_config.run.parameter_map
        ]
        logger.info(
            "Acquisition folders on which to run analysis:"
            f" {selected_acquisition_folders}"
        )

        # Keep a list of results as a container from the multiprocess jobs
        results = []
        acquisitions = []
        if parallel:
            cpu_count = cpu_count or multiprocessing.cpu_count()
            logger.info(f"Parallel enabled. cpus: {cpu_count}")
            if platform.system() == 'Windows':
                context = multiprocessing.get_context('spawn')
            else: 
                context = multiprocessing.get_context('forkserver')

            with ProcessPoolExecutor(
                max_workers=cpu_count,
                mp_context=context,
            ) as executor:
                jobs = {
                    (
                        executor.submit(
                            olefin.analysis.utils.get_acquisition_analysis,
                            parsed_config,
                            acquisition_folder,
                            acquisition_class,
                            event_class,
                            signal_class,
                        )
                    ): acquisition_folder
                    for acquisition_folder in selected_acquisition_folders
                }
                logger.debug(f"Created parallel jobs: {jobs}")
                with tqdm(total=len(jobs), desc="HV folders") as pbar:
                    for job_instance in as_completed(jobs):
                        logger.info(f"Job completed: {jobs[job_instance]}")
                        pbar.update(1)
                        try:
                            acquisition_analysis = job_instance.result()
                            acquisition_analysis_data = acquisition_analysis.data
                            results.append(acquisition_analysis_data)
                            if keep_data:
                                acquisitions.append(acquisition_analysis)
                        except Exception as exc:
                            logger.exception(
                                "%r generated an exception: %s" % (job_instance, exc)
                            )

        else:
            logger.info("Using single core for acquisition folder analysis")
            for acquisition_folder in tqdm(acquisition_folders, desc="HV folders"):
                logger.debug(f"Acquisition folder: {acquisition_folder}")
                acquisition_analysis = acquisition_class.from_folder(
                    config=parsed_config,
                    folder=acquisition_folder,
                    event_class=event_class,
                    signal_class=signal_class,
                )
                acquisition_analysis.run()
                results.append(acquisition_analysis.data)
                if keep_data:
                    acquisitions.append(acquisition_analysis)

        if results:
            # Concat the results from single acquisition analysis
            acquisition_data = pd.concat(results)
            # Sort values by folder num for better readability
            acquisition_data = acquisition_data.sort_values(by="folder_num")

            # Add the run_name columns and set it as index
            run_name = RunAnalysis.get_run_name_from_path(real_run_path)
            acquisition_data["run_name"] = run_name
            acquisition_data = acquisition_data.set_index("run_name", append=True)

            folder_num_serie = acquisition_data.index.get_level_values("folder_num")
            voltage_serie = folder_num_serie.map(parameter_map)

            # Correct data for pressure/temperature if possible
            p0 = parsed_config.run.p0 or 965
            t0 = (parsed_config.run.t0 or 20) + 273.15
            if not parsed_config.run.p or not parsed_config.run.t:
                logger.warning(
                    'p and t not set in "run" section of configs. Using default values'
                    " of 965 mbar and 20 °C"
                )
            p = parsed_config.run.p or 965
            t = (parsed_config.run.t or 20) + 273.15
            correction_factor = t / t0 * p0 / p
            logging.info(f"Applying correction factor (t, t0, p, p0): {(t, t0, p, p0)}")
            parsed_config.run.correction_factor = correction_factor
            acquisition_data["voltage_app"] = voltage_serie.values.round(decimals=0)
            acquisition_data["voltage"] = np.round(
                voltage_serie * correction_factor, decimals=0
            )
        else:
            acquisition_data = pd.DataFrame(
                columns=["run_name", "rpc", "voltage", "voltage_app"]
            )

        # Add rate values if presents
        # The presence is determined by the config attribute 'rate'.
        # If present, a rate analysis is performed and the rate values
        # are merged using the voltage values
        if config.rate:
            try:
                rate_analysis = rate_analysis_class.from_path(
                    real_run_path, parsed_config, parallel=parallel, keep_data=keep_data
                )
                rate_data = rate_analysis.indata
                acquisition_data = pd.merge(
                    acquisition_data,
                    rate_data,
                    left_on=["run_name", "rpc", "voltage", "voltage_app"],
                    right_on=["run_name", "rpc", "voltage", "voltage_app"],
                    how="inner" if results else "right",
                )
            except Exception as e:
                logging.error("Could not perform rate analysis:")
                logging.exception(e)

        # Add currents value if possible:
        # For now the only supported way is to read currents from the auto
        # generated file. The constraint is mainly on the structure of the
        # column and of the table.
        # Since multiple currents file might be present in the folder, the
        # currents file should be specified in the configs under the 'run'
        # section
        if parsed_config.run.currents_path:
            currents_path = Path(parsed_config.run.currents_path)
            # If the path is not absolute make it relative to the run path
            if not currents_path.is_absolute():
                currents_path = real_run_path / currents_path
            dfcurrents = olefin.analysis.utils.parse_currents_autogen(currents_path)
            # Merge the configs
            acquisition_data = pd.merge(
                acquisition_data.reset_index(),
                dfcurrents.reset_index(),
                left_on=["run_name", "rpc", "voltage_app"],
                right_on=["run", "RPC", "HV"],
                how="left",
            ).drop(columns=["RPC", "HV", "run"])

        # Set index on run name, rpc and hv
        acquisition_data = (
            acquisition_data.reset_index()
            .set_index(["run_name", "rpc", "voltage", "voltage_app"])
            .sort_index()
        )

        # Create the main class
        run_analysis = cls(config=parsed_config, raw_data=acquisition_data)
        # Add attributes to the class
        run_analysis.path = real_run_path
        run_analysis.keep_data = keep_data
        # If keep data is enabled attach the sub analysis dataframes
        if keep_data:
            run_analysis.acquisitions = acquisitions
            run_analysis.events = [acquisition.event for acquisition in acquisitions]
            event_data = pd.concat(
                (acquisition.event.data for acquisition in acquisitions)
            )
            signal_data = pd.concat(
                (acquisition.event.indata) for acquisition in acquisitions
            )
            # inplace assignment of a 'voltage' index column
            for data in [event_data, signal_data]:
                voltage_app = (
                    data.index.get_level_values("folder_num")
                    .map(parameter_map)
                    .rename("voltage_app")
                )
                voltage = (voltage_app * correction_factor).rename("voltage")
                data.set_index(
                    [voltage, voltage_app],
                    append=True,
                    inplace=True,
                )
            # Save acquisition, event, signal and currents data
            run_analysis.acquisition_data = olefin.AcquisitionAnalysisDataFrame(
                acquisition_data
            )
            run_analysis.event_data = olefin.EventAnalysisDataFrame(event_data)
            run_analysis.signal_data = signal_data
            if not dfcurrents.empty:
                run_analysis.currents_data = dfcurrents

        return run_analysis

    @staticmethod
    def efficiency_model(x, effmax, gamma, hv50):
        """
        The model to fit the efficiency.

        Parameters
        ----------
        params : list
            should be in the order (effmax, gamma, hv50)
        x : list
            the voltage values

        Returns
        -------
        float or np.ndarray
            The efficiency values
        """
        return effmax / (1 + np.exp(-gamma * (x - hv50)))

    @staticmethod
    def inverse_efficiency_model(params, y):
        """
        Inverse function of the sigmoid, i.e. from efficiency get voltage
        """
        return (params[1] * params[2] - np.log((params[0] - y) / y)) / params[1]

    @staticmethod
    def get_voltage_knee(effmax, gamma, hv50, effpoint=0.95):
        """
        Calculate the efficiency knee for a fixed efficiency point

        Parameters
        ---------
        effmax : float
            parameter from the efficiency model
        gamma : float
            parameter from the efficiency model
        hv50 : float
            parameter from the efficiency model
        effpoint : float
            the efficiency at which to calculate the voltage

        Returns
        -------
        float
            the voltage point (knee)
        """
        return RunAnalysis.inverse_efficiency_model(
            [effmax, gamma, hv50], effpoint * effmax
        )

    def estimate_feature(
        self, x, y, model, initial_params, x_estimate, xerr=None, yerr=None
    ):
        """
        Estimate an y value from a model given its x value.

        This function will use x,y values and they will be fitted together
        based on the  model provided. The fitted function will return the
        value of ``y`` given the x point ``x_estimate``.

        Parameters
        ----------
        x : np.ndarray
            the x values of data to fit
        y : np.ndarray
            the y values of data to fit
        model : callabe
            the function in the form ``f(params, x)`` to fit data
        initial_params : list or np.ndarray
            the initial params guess for fitting data
        x_estimate : int or float
            the x value at which to estimate f(x)
        xerr : np.ndarray optional
            an array of x errors
        yerr : np.ndarray optional
            an array of y errors

        Returns
        -------
        int or float
            the y value computed with f(params, x) where params are the fitted
            params
        """
        beta, sd_beta, res_var = olefin.analysis.utils.fit_model(
            x, y, model, initial_params, yerr
        )
        y_estimate = model(x_estimate, *beta)
        return y_estimate

    def fit_efficiency(
        self,
        hv: np.ndarray,
        eff: np.ndarray,
        eff_error: np.ndarray = None,
        hv_error: np.ndarray = None,
        initial_params: t.List[float] = [],
        bounds: t.Tuple[t.Tuple[float]] = ((0, 1e-4, 6000), (1, 0.2, 12000)),
        remove_zero_efficiency: bool = True,
    ) -> t.Dict:
        """
        Fit the efficiency to the given voltage and efficiency point with a
        sigmoid.

        Parameters
        ----------
        hv : np.ndarray
            the voltage values
        eff : np.ndarray
            the efficiency values
        eff_error : np.ndarray optional
            the error on the efficiency
        hv_error : np.ndarray optional
            the error on the voltage
        inital_params : list of float optional
            the initial parameters for the fit. Should be a list in the order
            (effmax, gamma, hv50). If not provided (0.9, 0.05, hvmax - 400) will
            be used
        bounds : list of tuple
            Limit the convergence of the parameters to the desired values
        remove_zero_efficiency: bool default True
            Discard values with efficiency equal to 0 to avoid infinite residuals
            in the minimization process

        Returns
        -------
        dict
            a dict containing the following information:
            - effmax
            - effmax_err
            - gamma
            - gamma_err
            - hv50
            - hv50_err
            - chi2
        """
        eff_error = eff_error if eff_error is not None else np.full_like(eff, 1)
        hv_error = hv_error if hv_error is not None else np.full_like(hv, 1)
        if remove_zero_efficiency:
            mask = eff != 0
            eff = eff[mask]
            hv = hv[mask]
            eff_error = eff_error[mask]
            hv_error = hv_error[mask]
        if not len(initial_params):
            logger.warning(
                "Initial parameter for fitting efficiency were not provided."
            )
            effmax = 0.9
            gamma = 0.01
            hv50 = hv.max() - 400
            initial_params = [effmax, gamma, hv50]
            logger.info(
                f"Trying to estimate parameters with [effmax={effmax}, gamma={gamma},"
                f" hv50={hv50}]"
            )

        params, errors, chi2 = olefin.analysis.utils.fit_model(
            hv,
            eff,
            RunAnalysis.efficiency_model,
            initial_params,
            eff_error,
            bounds=bounds,
        )
        return {
            "effmax": params[0],
            "effmax_err": errors[0],
            "gamma": params[1],
            "gamma_err": errors[1],
            "hv50": params[2],
            "hv50_err": errors[2],
            "chi2": chi2,
        }

    def calc_features(self, data):
        """
        Compute the features on run analysis.

        This method will use the provided ``data`` parameter to run the analysis.
        ``data`` should be a dataframe in the shape ((voltage, rpc), features).
        The `voltage` and `rpc` indexes are used to perform fit over data and
        groupby data by rpc.

        The features estimated by this method are:

        - knee: the voltage where efficiency = 0.95 effmax
        - working_point : the knee + config.run.working_point_delta or 200 V
        - streamer_probability: the streamer proability at working point
        - streamer_probability_error: the streamer proability error at working point

        Parameters
        ----------
        data : pd.DataFrame
            a dataframe in the shape ((voltage, rpc), features). There should be
            a multi index with index ``voltage`` and ``rpc`` present.

        Returns
        -------
        pd.DataFrame
            a dataframe of shape (rpc, features)
        """
        rpc_grouped_df = data.reset_index("voltage").groupby("rpc")
        # Search for initial params
        initial_params = self.config.run.initial_params
        res = rpc_grouped_df.apply(
            lambda x: pd.Series(
                self.fit_efficiency(
                    x.voltage,
                    x.efficiency,
                    x.efficiency_error,
                    initial_params=initial_params,
                    bounds=self.config.run.efficiency_parameters_limits,
                )
            )
        )

        res["knee"] = res.apply(
            lambda x: RunAnalysis.get_voltage_knee(
                x.effmax, x.gamma, x.hv50, effpoint=0.95
            ),
            axis=1,
        )
        res["working_point"] = res.knee + self.config.run.get(
            "working_point_delta", 200
        )
        derivative_gamma = np.log(100 / 95 - 1) / res.gamma**2
        derivative_hv50 = 1
        working_point_error = np.sqrt(
            (derivative_gamma * res.gamma_err) ** 2
            + (derivative_hv50 * res.hv50_err) ** 2
        )
        res["working_point_error"] = working_point_error
        for rpc, group in rpc_grouped_df:
            logging.info("Computing data in grouped df")
            stprob_model = self.efficiency_model
            fit_results = res.loc[rpc]
            working_point = fit_results.working_point
            initial_params_stprob = (
                fit_results.effmax,
                fit_results.gamma,
                fit_results.hv50 + 800,
            )

            def linear_model(x, m, q):
                return m * x + q

            params, sd_params, res_var = olefin.analysis.utils.fit_model(
                group.voltage,
                group.streamer_probability,
                stprob_model,
                initial_params_stprob,
                None,
            )
            stprob_wp = stprob_model(working_point, *params)
            res.loc[rpc, "streamer_probability"] = stprob_wp
            stprob_variability_lower = stprob_model(
                working_point - self.config.run.lower_voltage_streamer_variability,
                *params,
            )
            stprob_variability_upper = stprob_model(
                working_point + self.config.run.upper_voltage_streamer_variability,
                *params,
            )
            res.loc[rpc, "streamer_variability_lower"] = stprob_variability_lower
            res.loc[rpc, "streamer_variability_upper"] = stprob_variability_upper
            res.loc[rpc, "streamer_variability"] = (
                stprob_variability_upper - stprob_variability_lower
            )
            voltage_streamer_separation = self.inverse_efficiency_model(
                params, y=self.config.run.computed_streamer_probability_separation
            )
            voltage_reference_feature = res.loc[
                rpc, self.config.run.feature_streamer_probability_separation
            ]
            avst_separation = voltage_streamer_separation - voltage_reference_feature
            res.loc[rpc, "avalanche_steamer_separation"] = avst_separation

            stprob_err_wp = self.estimate_feature(
                group.voltage,
                group.streamer_probability_error,
                linear_model,
                x_estimate=working_point,
                initial_params=[1.0, 1.0],
            )
            res.loc[rpc, "streamer_probability_error"] = stprob_err_wp

        return res

    def run(self):
        """
        Run the analysis over the objec. This method will compute all the features
        from the .indata attributes and the returned value will be set on the
        .data attribute
        """
        self.data = self.calc_features(self.indata)
        # Add the run_df to olefin_plot() for it to take
        # fit params
        if hasattr(self, "acquisition_data"):
            self.acquisition_data.olefin_plot = partial(
                self.acquisition_data.olefin_plot, dfrun=self.data
            )

    def get_signals(self, queried_df: pd.DataFrame, repeating_variable: str = "event"):
        """Retrieve wave signal from an event or signal dataframe.

        This function returns a dataframe to get the wave signals from an event
        or signal dataframe. It does it by looking at the column of the dataframe
        and it assumes that the columns 'folder_num', 'voltage_app', and 'rpc'
        are in the index or in the columns. This should be by default if a standard
        run_analysis is created from path with keep_data=True.

        The return dataframe consists of the following columns:
        - time_sample: the sample (from 0 to record_length)
        - nevent or event: the number of the event
        - value: the adc value from the waveform
        - rpc
        - voltage_app
        - strip

        Parameters
        ----------
        queried_df: pd.DataFrame
            an event or signal dataframe containing 'folder_num', 'voltage_app',
            and 'rpc' either in the index or in the columns. Be careful to not select
            too many rows.
        repeating_variable: str
            the variable that represents the events in the waveform

        Returns
        -------
        pd.DataFrame
            A dataframe containing:
            The return dataframe consists of the following columns:
            - time_sample: the sample (from 0 to record_length)
            - nevent or event: the number of the event
            - value: the adc value from the waveform
            - rpc
            - voltage_app
            - strip
        """

        return olefin.analysis.utils.get_signals(
            queried_df, self.config, self.path, repeating_variable
        )

    def to_pickle(self, path=None):
        """
        Serialize the object with pickle

        Parameters
        ----------
        path : path or Path optional
            the path where to save the object. If not provided, the object will
            be saved in config.run.path with the name "olefin_analysis.pkl"

        Returns
        -------
        Path
            the path to the saved object
        """
        savepath = path or Path(self.config.run.path) / "olefin_analysis.pkl"
        savepath = Path(savepath)
        with open(savepath, "bw+") as f:
            pickle.dump(self, f)

        return savepath

    def to_excel(self, path: t.Union[str, Path] = None):
        """
        Serialize the class to excel.

        This method will try to save the analysed data into different excel
        sheets. In particular, it will always create a sheet named ``run``
        and a sheet named ``acquisition`` contianing the excel representation
        of the run.data and acquisition.data dataframe objects.

        If the class has ``.keep_data`` set to ``True`` then this method
        will try to create a sheet named ``event`` and a sheet named ``signal``
        with the analysed.
        **Warning**: saving ``event`` and ``signal`` objects may take minutes!

        Parameters
        ----------
        path : str or Path
            the path where to store the object. If not provided, config.run.path
            will be used and the excel file will be named "olefin_analysis.xlsx"

        Returns
        -------
        Path
            the path to the saved object
        """
        savepath = path or Path(self.config.run.path) / "olefin_analysis.xlsx"
        savepath = Path(savepath)
        with pd.ExcelWriter(savepath, mode="w") as writer:
            self.data.to_excel(writer, sheet_name="run")
            logger.info("Wrote run data into excel file")
            self.indata.to_excel(writer, sheet_name="acquisition")
            logger.info("Wrote acquisition data into excel file")
            if self.keep_data:
                self.event_data.to_excel(writer, sheet_name="event")
                logger.info("Wrote event_data into excel file")
                self.signal_data.to_excel(writer, sheet_name="signal")
                logger.info("Wrote signal_data into excel file")

        return savepath

    def to_json(self, path=None):
        """
        Serialize the object in a json format.

        The json object serialization is an attempt to store all the data
        into a readable text file. However, if the attribute ``keep_data`` of
        the class is set to ``True`` the created text file will be difficult
        to read with a text editor due to the large number of characters in
        a single line. For this reason this method should be used carefully.

        The stored json represnetation uses the pandas ``.to_dict()`` function
        with ``orient="split"`` so that both index, values and columns data
        are maintained.

        Parameters
        ----------
        path : str or Path
            the path where to store the object. If not provided, config.run.path
            will be used and the excel file will be named "olefin_analysis.json"

        Returns
        -------
        Path
            the path to the saved object
        """
        savepath = path or Path(self.config.run.path) / "olefin_analysis.json"
        savepath = Path(savepath)
        with open(savepath, "w+") as f:
            save_dict = {}
            save_dict["run"] = self.data.to_dict(orient="split")
            save_dict["analysis"] = self.indata.to_dict(orient="split")
            if self.keep_data:
                save_dict["event"] = self.event_data.to_dict(orient="split")
                save_dict["signal"] = self.signal_data.to_dict(orient="split")

            logger.info(f"Saving data into json file {savepath}")
            json.dump(save_dict, f)

        return savepath

    def __repr__(self):
        return repr(self.data)


class EPDTRunAnalysis(RunAnalysis):
    """Analysis at the highest (run) level.

    A run is a collection of acquisitions done on one or more RPCs. The class
    expects a raw_data parameter to be in the same shape of the results of a
    AcquisitionAnalysis class. This means raw_data should have a shape
    ((n_folders, n_rpcs), n_features) and the computed dataframe will be in the
    shape (n_rpc, n_features).

    Attributes
    ---------
    data : pd.DataFrame
        the data resulted from the .run() method
    config : olefin.config
        the global configuration object
    """

    @classmethod
    def from_path(
        cls,
        run_path: t.Union[str, Path],
        config=None,
        top_folder="no-header-512",
        config_pattern="**/olefin_config.*",
        parallel=True,
        cpu_count=None,
        keep_data=False,
        parameter_file_pattern="**/[pP]arameters.dat",
        parameter_column_map={1: "folder_num", 2: "voltage"},
        acquisition_class=olefin.EPDTAcquisitionAnalysis,
        event_class=olefin.EPDTEventAnalysis,
        signal_class=olefin.EPDTSignalAnalysis,
    ):
        return super(EPDTRunAnalysis, cls).from_path(
            run_path,
            config,
            top_folder,
            config_pattern,
            parallel,
            cpu_count,
            keep_data,
            parameter_file_pattern,
            parameter_column_map,
            acquisition_class,
            event_class,
            signal_class,
        )

    def to_pickle(self, path=None):
        """
        Serialize the object with pickle

        Parameters
        ----------
        path : path or Path optional
            the path where to save the object. If not provided, the object will
            be saved in config.run.path with the name "olefin_analysis.pkl"

        Returns
        -------
        Path
            the path to the saved object
        """
        savepath = path or Path(self.config.run.path) / "olefin_analysis.pkl"
        savepath = Path(savepath)
        with open(savepath, "bw+") as f:
            pickle.dump(self, f)

        return savepath

    def calc_features(self, data):
        """
        Compute the features on run analysis.

        This method will use the provided ``data`` parameter to run the analysis.
        ``data`` should be a dataframe in the shape ((voltage, rpc), features).
        The `voltage` and `rpc` indexes are used to perform fit over data and
        groupby data by rpc.

        The features estimated by this method are:

        - knee: the voltage where efficiency = 0.95 effmax
        - working_point : the knee + config.run.working_point_delta or 200 V
        - streamer_probability: the streamer proability at working point
        - streamer_probability_error: the streamer proability error at working point

        Parameters
        ----------
        data : pd.DataFrame
            a dataframe in the shape ((voltage, rpc), features). There should be
            a multi index with index ``voltage`` and ``rpc`` present.

        Returns
        -------
        pd.DataFrame
            a dataframe of shape (rpc, features)
        """
        rpc_grouped_df = data.reset_index("voltage").groupby("rpc")
        res = data
        # Search for initial params
        if "efficiency" in data:
            initial_params = self.config.run.initial_params
            res = rpc_grouped_df.apply(
                lambda x: pd.Series(
                    self.fit_efficiency(
                        x.voltage,
                        x.efficiency,
                        x.efficiency_error,
                        initial_params=initial_params,
                        bounds=self.config.run.efficiency_parameters_limits,
                    )
                )
            )

            res["knee"] = res.apply(
                lambda x: EPDTRunAnalysis.get_voltage_knee(
                    x.effmax, x.gamma, x.hv50, effpoint=0.95
                ),
                axis=1,
            )
            res["working_point"] = res.knee + self.config.run.get(
                "working_point_delta", 200
            )
            derivative_gamma = np.log(100 / 95 - 1) / res.gamma**2
            derivative_hv50 = 1
            res["working_point_error"] = np.sqrt(
                (derivative_gamma * res.gamma_err) ** 2
                + (derivative_hv50 * res.hv50_err) ** 2
            )

            for rpc, group in rpc_grouped_df:
                logging.info("Computing data in grouped df")
                rpc_fit_data = res.loc[rpc]
                working_point = rpc_fit_data.working_point
                voltage = group.voltage

                features_list = [
                    "avalanche",
                    "streamer",
                    "streamer_probability",
                    "prompt_charge",
                    "time_res",
                    "cluster_size",
                    "cluster_size_avalanche",
                    "cluster_size_streamer",
                    "height",
                    "time_over_threshold",
                ]
                if "rate" in data.columns:
                    features_list.append("rate")

                # Interpolate standard features with their errors on
                # run.data
                for feature in features_list:
                    interp_func = si.interp1d(
                        group.voltage, group[feature], fill_value="extrapolate"
                    )
                    y_interp = interp_func(working_point)
                    res.loc[rpc, feature] = y_interp
                    interp_func_err = si.interp1d(
                        group.voltage,
                        group[f"{feature}_error"],
                        fill_value="extrapolate",
                        kind=self.config.run.interpolation_mode,
                    )
                    y_interp_err = interp_func_err(working_point)
                    res.loc[rpc, f"{feature}_error"] = y_interp_err

                try:
                    # Compute streamer features
                    # Inverse interpolation voltage - st.prob. to obtain
                    # Voltage from a known stprob value
                    inverse_stprob_model = si.interp1d(
                        group.streamer_probability,
                        group.voltage,
                        fill_value="extrapolate",
                        kind=self.config.run.interpolation_mode,
                    )

                    voltage_streamer_separation = inverse_stprob_model(
                        self.config.run.computed_streamer_probability_separation
                    )
                    voltage_reference_feature = res.loc[
                        rpc, self.config.run.feature_streamer_probability_separation
                    ]
                    avst_separation = (
                        voltage_streamer_separation - voltage_reference_feature
                    )
                    res.loc[rpc, "avalanche_streamer_separation"] = avst_separation
                except ValueError:
                    res.loc[rpc, "avalanche_streamer_separation"] = np.nan

                stprob_model = si.interp1d(
                    group.voltage,
                    group.streamer_probability,
                    fill_value="extrapolate",
                    kind=self.config.run.interpolation_mode,
                )

                stprob_variability_lower = stprob_model(
                    working_point - self.config.run.lower_voltage_streamer_variability
                )
                stprob_variability_upper = stprob_model(
                    working_point + self.config.run.upper_voltage_streamer_variability
                )
                res.loc[rpc, "streamer_variability_lower"] = stprob_variability_lower
                res.loc[rpc, "streamer_variability_upper"] = stprob_variability_upper
                res.loc[rpc, "streamer_variability"] = (
                    stprob_variability_upper - stprob_variability_lower
                )

                # Currents are special features as they may be multiple,
                # share error between cosmics and beam and may not exist
                currents_features = [
                    "currents_standard_mean",
                    "currents_standard_std",
                    "currents_beam_mean",
                ]
                for currents_feature in currents_features:
                    if currents_feature not in group.columns:
                        continue
                    currents_interpolated = olefin.analysis.utils.interpolate(
                        voltage, group[currents_feature], working_point
                    )
                    res.loc[rpc, currents_feature] = currents_interpolated

        return res

    def get_signals(self, queried_df: pd.DataFrame, repeating_variable: str = "nevent"):
        """Overridden method of the run analysis with the default repeating_variable
        set to 'nevent' instead of 'event'"""
        return super().get_signals(queried_df, repeating_variable)
