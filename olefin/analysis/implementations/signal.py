import olefin
from olefin.analysis.interface import BaseAnalysis
import numpy as np
import pandas as pd
import typing as t
from pathlib import Path
import logging
from scipy.signal import savgol_filter, find_peaks

np.seterr(divide="ignore")
logger = logging.getLogger(__name__)
logging.basicConfig(
    format=(
        "%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)s -"
        " %(message)s"
    ),
)


class SignalAnalysis(BaseAnalysis):
    """
    Lowest level analysis at signal level.

    This analyses data from a single signals and extract the physical
    variables. The physical variables are called features of the signal
    and are usually a scalar.

    The class expectes an :obj:`np.ndarray` 2D array where the rows represents
    the sample number and the column represent the sample number of a particular
    event.

    Upon object initialization the raw_data 2D array is stored as the .indata
    attribute of the object.

    When the .run() method is called on the object the .data attribute will
    be filled with a pandas dataframe representing the results of the analysis.

    The structure of the .data DataFrame is the follwing:
        self.data : pd.DataFrame of shape ``(n_events, n_features)``
        ``
        index: [0, 1, ... n_events]
        feature1: [f1[event0], f1[event1], f1[event2], ...f1[n_events]]
        featurem: [fm[event0], fm[event1], fm[event2], ...fm[n_events]]
        ``

    The class should call the .run() method to perform analysis on the raw_data


    Attributes
    ----------
    data : None or :obj:`pd.DataFrame`
    config : None or :obj:`Box`
    """

    data = None
    config = None

    def __init__(self, config: "olefin.config", data: np.ndarray, axis: int = 1):
        """
        Create a SignalAnalysis object.

        Parameters
        ----------
        config : Box
            A config object of class Box with the proper values used by the class.
            The list of required config object for this particular implementations
            are:
                TODO: document
        data : np.ndarray
            A 2D array of shape ``(n_events, n_samples)``

        """
        self.config = config
        if axis not in [0, 1]:
            raise ValueError(f"Value of axis should be in [0, 1]. {axis} was passed")
        self.axis = axis
        self.indata = data  # indata = input data
        self.logger = logging.getLogger("SignalAnalysis")

    @staticmethod
    def read_wavefile(
        wavefile: t.Union[str, Path],
        record_length: int,
        axis: int = 1,
        header_lines: int = None,
    ):
        """
        Read values from a wavefile into a 2D numpy array.

        This function reads the content of a wave<n>.txt file without headers,
        parse it and return a 2D array of shape ``(n_events, n_samples)``.

        Parameters
        ----------
        wavefile: str or Path
            The path to the wavefile
        record_length: int
            the record length parameter defined in when used the wavedump program

        Returns
        -------
        np.ndarray
            The parsed 2D numpy array
        """
        if axis == 1:
            shape = (-1, record_length)
        elif axis == 0:
            shape = (record_length, -1)
        logger.debug(f"Reading wavefile {wavefile}")
        if header_lines and isinstance(header_lines, int):
            raw_values = pd.read_csv(
                wavefile,
                header=None,
                skiprows=lambda x: x % (record_length + header_lines) < header_lines,
                names=["wave"],
            ).wave.values.reshape(shape)
        else:
            raw_values = pd.read_csv(
                wavefile, header=None, names=["wave"]
            ).wave.values.reshape(shape)
        return raw_values

    @classmethod
    def from_wavefile(
        cls,
        config: olefin.analysis.configuration.config,
        wavefile: t.Union[str, Path],
        record_length: int = None,
        axis: int = 1,
    ):
        """
        Create a SignalAnalysis class from a wavefile.

        This function calls the :obj:`read_wavefile` function and uses it to
        initalize a SignalAnalysis object.

        Parameters
        ----------
        config : Box
            the config object used to perform analysis
        wavefile : str or Path
            the path to the wavefile
        record_length: int or None
            the record length of the wavefile. If not set the function will try
            to search the record_length from the config object
        axis : int
            either 0 or 1. If 0 the shape of the parsed array will be
            (n_events, n_samples). If 1 it will be (n_samples, n_events).

        Returns
        -------
        SignalAnalysis
            an object with the .data attribute set by the parsed wavefile
        """

        # Merge and update config from base configs
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        rl = record_length or parsed_config.acquisition.record_length
        if not rl:
            raise ValueError(
                "record length from arg and from config is None but should be int"
            )
        raw_values = cls.read_wavefile(
            wavefile, rl, axis, parsed_config.acquisition.get("header_lines")
        )
        logger.debug(f"Creating SignalAnalysis from wavefeile. Record length: {rl}")
        return cls(config=parsed_config, data=raw_values, axis=axis)

    def get_events(
        self,
        events: t.Union[t.Union[t.List[int], pd.Series, np.ndarray], int, str] = "all",
    ):
        """
        Returns event data.

        This functions accepts events and will returns all the samples corresponding
        to the provided events as a 2D numpy array of shape (n_provided_events, n_samples)

        Parameters
        ----------
        events : int or list of int or str
            The events to get. If "all" is supplied, all the events will be returned.


        Returns
        -------
        np.ndarray
            A 2D array of shape (n_provided_events, n_samples)
        """
        if isinstance(events, str) and events == "all":
            return self.indata
        elif isinstance(events, (list, np.ndarray, pd.Series, int)):
            return self.indata[events, :]
        else:
            raise ValueError(
                f"events should be int, pd.Series, np.ndarray but {type(events)} was"
                " supplied."
            )

    def calc_features(
        self,
        data: np.ndarray,
        axis: int = 1,
        trigger_times=None,
        resistance=None,
        *args,
        **kwargs,
    ):
        """
        Calculate features on signal data.

        Signal data is a :obj:`np.ndarray` of shape (n_events, n_samples).
        This will make the .data attribute available and return it at the
        same time.

        This function will compute features with vectorized functions and
        return them in form of a :obj:`pd.DataFrame` with shape (n_events, n_features)
        with each column named after the computed feature.

        The computed features in this implementations are:
        - baseline_mean: the baseline mean value
        - baseline_rms: the baseline std or rms value
        - time_peak: the where the biggest peak is found.
        - tot_start: the time where the signals crosses the
            baseline_rms * config.signal.baseline_threshold. 0 if signal
            doesn't cross threshold
        - tot_stop: the last time where the signals comes back below threshold.
        - tot: the difference tot_stop - tot_start
        - height: the height in mV of the peak
        - inverse_height: the inverse height of the signal
        - charge: the sum over time of the samples substracted by the mean baseline
        - fired: boolean indicating if height > threshold and charge > config.signal.charge_threshold
        - ratio: the ratio between height and inverse height
        - signal_type: the type of the signal, from utils.SignalTypes.
            -2 indicate something not categorized, -1 a false positive, 0 indicate
            noise, 1 an avalanche signal, 2 extra charge signal, 3 streamer signal

        Parameters
        ----------
        data : np.ndarray
            signal data in the shape of (n_events, n_samples)
        axis : int
            axis over which compute the features of the signal. Default is 1,
            which is the axis over which samples are present
        trigger_times : np.ndarray
            array with the times of the trigger in the shape of (n_events)
        resistance : optional float
            resistance used to compute charge values. If None, the value from
            config.rpc.resistance will be used

        Returns
        -------
        pd.DataFrame
            The dataframe in the shape (n_events, n_features) of the features
        """
        config = self.config
        bstart, bstop = (
            config.signal.baseline_interval[0],
            config.signal.baseline_interval[1],
        )
        data = data * config.digitizer.adc_to_mv
        baseline_mean = data[:, bstart:bstop].mean(axis=axis)
        # Use positive pulse signal because it's easier
        # to think about the operations to apply
        signum = 1 if config.signal.positive_pulse_polarity else -1
        normdata = signum * (data - baseline_mean[:, None])
        baseline_region_data = normdata[:, bstart:bstop]
        signal_region_data = normdata[:, bstop:]
        baseline_rms = baseline_region_data.std(axis=axis)
        time_peak = signal_region_data.argmax(axis=axis)
        if trigger_times is not None:
            time_peak = time_peak - trigger_times
        threshold = baseline_rms * config.signal.baseline_threshold
        samples_over_threshold = signal_region_data > threshold[:, None]
        # To find the first occurene of a sample over threshold we search for
        # the argmax() of samples_over_threshold.
        # The array samples_over_threshold looks like [0, 0, 1, 1, 0, 1, ...]
        # argmax() results in the index of first occurence where the 1 is present
        tot_start = samples_over_threshold.argmax(axis=axis)
        # to get the last occurence instead we need to use a trick and
        # create a view of the reversed array with [::-1]
        # imagine the array [0, 1, 1, 0, 1, 0]
        # the last occurence of 1 is at index 4
        # the reverse is:   [0, 1, 0, 1, 1, 0]
        # argmax() on the reverse results in  index = 1
        # the original index would be: length of array (=6) - index (=1) - 1 = 4
        tot_stop = (
            samples_over_threshold.shape[1]
            - samples_over_threshold[:, ::-1].argmax(axis=axis)
            - 1
        )
        tot = (tot_stop - tot_start) * config.digitizer.time_resolution
        height = signal_region_data.max(axis=axis)
        inverse_height = signal_region_data.min(axis=axis)
        charge = (
            signal_region_data.sum(axis=axis)
            * config.digitizer.time_resolution
            / (resistance or config.rpc.resistance)
        )
        ratio = np.abs(height) / np.abs(inverse_height)
        fired = height > threshold
        tot_low, tot_high = config.signal.tot_region
        charge_low, charge_high = config.signal.charge_region
        signal_type = np.select(
            [
                (tot < tot_low)
                | (charge < charge_low)
                | ((charge > charge_high) & (tot > tot_low) & (tot < tot_high)),
                (tot >= tot_low) & ((charge >= charge_low) & (charge <= charge_high)),
                (tot >= tot_high) & (charge > charge_high),
            ],
            [
                olefin.SignalTypes.avalanche,
                olefin.SignalTypes.extra_charge,
                olefin.SignalTypes.streamer,
            ],
        )
        df = pd.DataFrame(
            {
                "baseline_mean": baseline_mean,
                "baseline_rms": baseline_rms,
                "time_peak": time_peak,
                "tot_start": tot_start,
                "tot_stop": tot_stop,
                "tot": tot,
                "height": height,
                "inverse_height": inverse_height,
                "charge": charge,
                "fired": fired,
                "ratio": ratio,
                "signal_type": signal_type,
            }
        )
        # Add trigger times data if enabled
        if trigger_times is not None:
            df["trigger_times"] = trigger_times
        self.logger.debug(f"Calculated signal features: {df.columns.tolist()}")
        return df

    def run(self, trigger_times=None, resistance=None, *args, **kwargs) -> pd.DataFrame:
        """
        Run the analysis by computing the features. Also sets the .data
        attribute

        Returns
        -------
        pd.DataFrame
            the dataframe with the features
        """
        # preprocess: get data
        features_data = self.calc_features(
            self.indata,
            self.axis,
            trigger_times=trigger_times,
            resistance=resistance,
            *args,
            **kwargs,
        )
        self.data = features_data
        return self.data


class EPDTSignalAnalysis(SignalAnalysis):
    """Subclass of the SignalAnalysis class with a behaviour
    in the computing of the features similar to the one of the legacy
    olefin package. Check this link for the legacy implementation:
    [https://gitlab.cern.ch/gasteam/olefin/-/tree/93748a0b8c42b8fedb64c4906edbc19b4892898f/olefin/legacy]

    This class should implement the same features of the olefin v1 functions

    """

    def __init__(
        self,
        config: "olefin.config",
        data: np.ndarray,
        axis: int = 1,
        *args,
        **kwargs,
    ):
        super().__init__(config, data, axis)

    @classmethod
    def from_wavefile(
        cls,
        config: olefin.analysis.configuration.config,
        wavefile: t.Union[str, Path],
        record_length: int = None,
        axis: int = 1,
    ):
        """
        Create a SignalAnalysis class from a wavefile.

        This function calls the :obj:`read_wavefile` function and uses it to
        initalize a SignalAnalysis object.

        Parameters
        ----------
        config : Box
            the config object used to perform analysis
        wavefile : str or Path
            the path to the wavefile
        record_length: int or None
            the record length of the wavefile. If not set the function will try
            to search the record_length from the config object
        axis : int
            either 0 or 1. If 0 the shape of the parsed array will be
            (n_events, n_samples). If 1 it will be (n_samples, n_events).

        Returns
        -------
        SignalAnalysis
            an object with the .data attribute set by the parsed wavefile
        """

        # Merge and update config from base configs
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        rl = record_length or parsed_config.acquisition.header
        if not rl:
            raise ValueError(
                "record length from arg and from config is None but should be int"
            )
        raw_values = cls.read_wavefile(
            wavefile, rl, axis, parsed_config.acquisition.get("header_lines")
        )
        logger.debug(f"Creating SignalAnalysis from wavefeile. Record length: {rl}")
        return cls(config=parsed_config, data=raw_values, axis=axis)

    def calc_features(
        self,
        data: np.ndarray,
        axis: int = 1,
        trigger_times=None,
        resistance=None,
        *args,
        **kwargs,
    ):
        """
        Calculate features on signal data. This method overrides the signalAnalysis
        method

        Signal data is a :obj:`np.ndarray` of shape (n_events, n_samples).
        This will make the .data attribute available and return it at the
        same time.

        This function will compute features with vectorized functions and
        return them in form of a :obj:`pd.DataFrame` with shape (n_events, n_features)
        with each column named after the computed feature.

        The computed features in this implementations are:
        - baseline: the baseline mean value
        - time_peak: the time (in adc sample) of the signal's peak
            (equivalent to time_minimum)
        - baseline: the baseline mean value
        - height: the height in mV of the peak
        - inverse_height: the inverse height of the signal
        - charge: the sum over time of the samples below a defined threshold
        - heights_ratio: the ratio between height and inverse height
        - time_peak: the where the biggest peak is found.
        - reflections_count: The number of samples crossing the inverse baseline
        - fired: boolean indicating if height > threshold
        - isin_time_window: if the minimum is within time_min and time_max
        - might_be_noise: if the reflections count > noise_counts
        - is_signal: boolean if is fired, not might_be_noise and isin_time_window
        - is_fat_signal: if is_signal and heights_ratio is lower than heights_ratio_threshold and
            the total height is bigger than a heights_difference_threshold
        - is_detected: if is_signal but not fat signal
        - signal_type: the type of the signal, from utils.SignalTypes.
        - type: -2 for uncategorized, 1 for avalanche and 3 for streamer, depending on
            charge_thresh and height_thresh and if it is detected

        Parameters
        ----------
        data : np.ndarray
            signal data in the shape of (n_events, n_samples)
        axis : int
            axis over which compute the features of the signal. Default is 1,
            which is the axis over which samples are present
        trigger_times: np.ndarray or pd.Series optional
            the times to use a reference to calculate the time based features.
            It should be a 1D array with the shape of the number of events

        Returns
        -------
        pd.DataFrame
            The dataframe in the shape (n_events, n_features) of the features
        """
        config = self.config
        time_min = config.signal.time_min
        time_max = config.signal.time_max
        # baseline start and stop
        bstart, bstop = (
            config.signal.baseline_interval[0],
            config.signal.baseline_interval[1],
        )
        # Compute the mean baseline for each different event
        baseline_mean = data[:, bstart:bstop].mean(axis=axis)
        # Use positive pulse signal because it's easier
        # to think about the operations to apply
        signum = 1 if config.signal.positive_pulse_polarity else -1
        normdata = signum * (data - baseline_mean[:, None]) * config.digitizer.adc_to_mv
        # Get the time of the first sample crossing threshold
        tinds = np.argmax(normdata > config.signal.height_thresh, axis=axis)
        nevents = np.arange(normdata.shape[0])
        # Compute the time peak as the x-intersection between the signal
        # and the baseline (intersection between 2 points and a line)
        time_peak = (config.signal.height_thresh - normdata[nevents, tinds - 1]) / (
            normdata[nevents, tinds] - normdata[nevents, tinds - 1]
        ) + tinds
        # Time over threhsold calclulation:
        samples_over_threshold = normdata > config.signal.height_thresh
        # To find the first occurene of a sample over threshold we search for
        # the argmax() of samples_over_threshold.
        # The array samples_over_threshold looks like [0, 0, 1, 1, 0, 1, ...]
        # argmax() results in the index of first occurence where the 1 is present
        tot_start = samples_over_threshold.argmax(axis=axis)
        # to get the last occurence instead we need to use a trick and
        # create a view of the reversed array with [::-1]
        # imagine the array [0, 1, 1, 0, 1, 0]
        # the last occurence of 1 is at index 4
        # the reverse is:   [0, 1, 0, 1, 1, 0]
        # argmax() on the reverse results in  index = 1
        # the original index would be: length of array (=6) - index (=1) - 1 = 4
        tot_stop = (
            samples_over_threshold.shape[1]
            - samples_over_threshold[:, ::-1].argmax(axis=axis)
            - 1
        )
        tot = (tot_stop - tot_start) * config.digitizer.time_resolution

        if trigger_times is not None:
            time_peak = time_peak - trigger_times
        height = normdata.max(axis=axis)
        inverse_height = -normdata.min(axis=axis)
        charge = (
            np.where(
                normdata > config.signal.charge_thresh * config.digitizer.adc_to_mv,
                normdata,
                0,
            ).sum(axis=axis)
            * config.digitizer.time_resolution
            / (resistance or config.rpc.resistance)
        )
        heights_ratio = np.abs(height) / np.abs(inverse_height)
        reflections_count = (
            normdata[:, time_min:time_max]
            < -config.signal.noise_thresh * config.digitizer.adc_to_mv
        ).sum(axis=axis)
        fired = height > config.signal.height_thresh
        if trigger_times is not None:
            time_min = time_min - trigger_times
            time_max = time_max - trigger_times
        isin_time_window = (time_peak > time_min) & (time_peak < time_max)
        might_be_noise = reflections_count >= config.signal.noise_counts
        is_signal = fired & isin_time_window
        is_fat_signal = is_signal & (
            heights_ratio < config.signal.heights_ratio_threshold
        )
        is_detected = is_signal & ~is_fat_signal
        signal_type = np.select(
            [
                ~is_detected,
                is_detected
                & (charge > config.signal.charge_thresh_av)
                & (height > config.signal.height_thresh_av),
            ],
            [olefin.SignalTypes.uncategorized, olefin.SignalTypes.streamer],
            default=olefin.SignalTypes.avalanche,
        )
        df = pd.DataFrame(
            {
                "baseline": baseline_mean,
                "height": height,
                "inverse_height": inverse_height,
                "charge": charge,
                "heights_ratio": heights_ratio,
                "time_peak": time_peak,
                "reflections_count": reflections_count,
                "fired": fired,
                "isin_time_window": isin_time_window,
                "might_be_noise": might_be_noise,
                "is_signal": is_signal,
                "is_fat_signal": is_fat_signal,
                "is_detected": is_detected,
                "type": signal_type,
                "time_over_threshold": tot,
            }
        )
        # Add trigger times data if enabled
        if trigger_times is not None:
            df["trigger_times"] = trigger_times

        self.logger.debug(f"Calculated signal features: {df.columns.tolist()}")
        return df


class TriggerAnalysis(SignalAnalysis):
    indata = None
    config = None

    def __init__(self, config, data, axis: int = 1):
        self.config = config
        self.indata = data
        self.axis = axis

    def calc_features(self, data: np.ndarray, axis: int = 1, *args, **kwargs):
        """Compute the features from trigger data.
        Features will be a single column dataframe with the trigger times.

        Trigger times are computed as the crossing falling edge of the trigger
        signal. If two trigger signals are present only the time of the falling
        edge of the first one is returned.

        Parameters
        ----------
        data: np.ndarray
            the 2D array where rows are events and columns the samples
        axis: int = 1
            the axis over which to perform vectorized operations

        Returns
        -------
        pd.DataFrame
            a dataframe with the columns:
            - 'trigger_times': float column of the time in samples where the
            falling edge crosses the half of the signal (max - min)

        """
        d = data
        # Find the max and min of each signals and take the middle value
        minvals, maxvals = d.min(axis=axis), d.max(axis=axis)
        midvals = (maxvals - minvals) / 2 + minvals
        # Add an extra dimension to perform vectorized indexing later
        midvals_extra_dimension = midvals[:, np.newaxis]
        # Use a one-off slicing to detect a falling edge:
        # https://stackoverflow.com/questions/50365310/python-rising-falling-edge-oscilloscope-like-trigger
        slice_before = d[:, :-1]
        slice_after = d[:, 1:]
        # Find the first occurence of the falling edge. The alternative would
        # be np.nonzero but it might return all the falling edge and if two
        # signals are present we might have a double value for a fixed event
        yinds = np.argmax(
            (slice_before > midvals_extra_dimension)
            & (slice_after < midvals_extra_dimension),
            axis=axis,
        )
        xinds = np.arange(len(d))
        # Calculate the crossing threshold by using a simple linear intersection
        # formula between a line and a point.
        xs1, xs2 = yinds, yinds + 1
        ys1, ys2 = d[xinds, xs1], d[xinds, xs2]
        ms = (ys2 - ys1) / (xs2 - xs1)
        qs = ys1 - ms * xs1
        xmidvals = (midvals - qs) / ms
        return pd.DataFrame({"times": xmidvals, "heights": midvals})

    def run(self):
        features_data = self.calc_features(self.indata, self.axis)
        self.data = features_data
        return self.data


class RateSignalAnalysis(SignalAnalysis):
    indata = None
    config = None

    def __init__(self, config, data, axis: int = 1):
        self.config = config
        self.indata = data
        self.axis = axis

    @classmethod
    def from_wavefile(
        cls,
        config: "olefin.analysis.configuration.config",
        wavefile: t.Union[str, Path],
        record_length: int = None,
        axis: int = 1,
    ):
        """
        Create a SignalAnalysis class from a wavefile.

        This function calls the :obj:`read_wavefile` function and uses it to
        initalize a SignalAnalysis object.

        Parameters
        ----------
        config : Box
            the config object used to perform analysis
        wavefile : str or Path
            the path to the wavefile
        record_length: int or None
            the record length of the wavefile. If not set the function will try
            to search the record_length from the config object
        axis : int
            either 0 or 1. If 0 the shape of the parsed array will be
            (n_events, n_samples). If 1 it will be (n_samples, n_events).

        Returns
        -------
        SignalAnalysis
            an object with the .data attribute set by the parsed wavefile
        """

        # Merge and update config from base configs
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        rl = record_length or parsed_config.rate.record_length
        if not rl:
            raise ValueError(
                "record length from arg and from config is None but should be int"
            )
        raw_values = cls.read_wavefile(
            wavefile, rl, axis, parsed_config.acquisition.get("header_lines")
        )
        logger.debug(f"Creating RateSignalAnalysis from Wavefile. Record length: {rl}")
        return cls(config=parsed_config, data=raw_values, axis=axis)

    def calc_features(self, data: np.ndarray, axis: int = 1, *args, **kwargs):
        """Compute the features from rate signal data.

        Rate signal data is in the form of 1 or few events with a large
        acquisition window. For each acquisition window the number of peaks is
        returned as main feature.

        Parameters
        ----------
        data: np.ndarray
            the 2D array where rows are events and columns the samples
        axis: int = 1
            the axis over which to perform vectorized operations

        Returns
        -------
        pd.DataFrame
            a dataframe with the columns:
            - 'position': the time in samples where the peak is found
            - 'height': the height of the peak

        """
        config = self.config
        data = data * config.digitizer.adc_to_mv
        baseline_mean = data.mean(axis=axis)
        # Use positive pulse signal because it's easier
        # to think about the operations to apply
        normdata = -(data - baseline_mean[:, None])
        # Smooth data before finding peaks. This avoids to count reflections
        smoothed = savgol_filter(
            normdata,
            window_length=config.rate.window_length,
            polyorder=config.rate.polyorder,
            axis=axis,
        )

        # Ravel the data in case multiple acquisition are present
        ravelled = np.ravel(smoothed)

        # The window size in samples is calculated from the config that indicates
        # the percentage.
        window_size = int(len(ravelled) * config.rate.window_size)
        # Calculate the step size each window should be rolled
        step_size = len(ravelled) // config.rate.n_windows
        # Create a 2D indexer
        indexer = (
            np.arange(window_size - 1)[None, :]
            + step_size * np.arange(config.rate.n_windows)[:, None]
        )
        rolling_window = np.take(ravelled, indexer, mode="wrap")

        num_peaks = []
        # Perform rate calculation for each rolled window
        for waveform in rolling_window:
            peak_positions, _ = find_peaks(
                waveform,
                height=config.rate.height,
                distance=config.rate.distance,
                prominence=config.rate.prominence,
            )
            num_peaks.append(len(peak_positions))

        count = np.array(num_peaks)
        # Calculate the error from poisson statistics: sigma = sqrt(mean_value/size_sample)
        count_err = np.sqrt(count / rolling_window.shape[0])
        # In Hz/cm2
        rate = (
            count
            / config.rate.area
            / (config.digitizer.time_resolution * 1e-9 * rolling_window.shape[1])
        )
        rate_err = np.sqrt(rate / rolling_window.shape[0])
        df = pd.DataFrame(
            {
                "count": count,
                "count_error": count_err,
                "rate": rate,
                "rate_error": rate_err,
            }
        )
        return df

    def run(self, trigger_times=None, *args, **kwargs) -> pd.DataFrame:
        """
        Run the analysis by computing the features. Also sets the .data
        attribute

        Returns
        -------
        pd.DataFrame
            the dataframe with the features
        """
        # preprocess: get data
        features_data = self.calc_features(
            self.indata, self.axis, trigger_times=trigger_times, *args, **kwargs
        )
        self.data = features_data
        return self.data
