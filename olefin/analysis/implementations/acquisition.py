import olefin
from olefin.analysis.interface import BaseAnalysis
import numpy as np
import pandas as pd
import typing as t
from pathlib import Path
import logging
import matplotlib as mpl
import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)
logging.basicConfig(
    format=(
        "%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)s -"
        " %(message)s"
    ),
)


class AcquisitionAnalysis(BaseAnalysis):
    """Analysis at acquisition Level.

    An acquisition is the output of the wavedump program. Usually the
    acquisition level corresponds with a fixed HV point. A wavedump program
    can write txt files in the file system named wave<n>.txt where ``n`` is the
    number of the channel connected to the digitizer. An acquisition class
    takes a raw_data as input in the form of a pandas DataFrame of shape
    ((n_folders,n_rpc,n_events), n_features) with a mult index (folder, rpc, event)
    and compute acquisition relatd features, i.e. features depending on a single
    wavedump acquisition (such as efficiency or streamer probability for example).

    Attributes
    ----------
    data : pd.DataFrame
        the dataframe used by the class to run the analysis
    config : olefin.config
        the global config object
    """

    data = None
    config = None

    def __init__(self, config: "olefin.config", raw_data):
        self.config = config
        self.indata = raw_data

    @classmethod
    def from_folder(
        cls,
        config: "olefin.config",
        folder: t.Union[str, Path],
        event_class=olefin.EventAnalysis,
        signal_class=olefin.SignalAnalysis,
    ):
        """
        Create an acquisition analysis from a wavedump output folder.

        The folder is the result of a wavedump acquisition, containing wave<n>.txt
        files. The class allows to pass custom classes for signal and event analysis.
        Also, the class will store the event analysis on the attribute .event
        so that it can be easily accessed for inspecting event data
        """

        # Merge and update config from base configs
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        logger.debug(f"Creating Acquistion class from folder: {folder}")
        event_analysis = event_class.from_folder(parsed_config, folder, signal_class)
        event_analysis.run()
        logger.debug(f"Created Event from Acquisition factory: {folder}")
        acquisition_analysis = cls(parsed_config, event_analysis.data)
        acquisition_analysis.event = event_analysis
        return acquisition_analysis

    def calc_features(self, data) -> pd.DataFrame:
        """
        Calculate acquisition features.

        ``data`` should be a pandas DataFrame with shape
        ((n_folders, n_rpcs, n_events), n_features) with a multi index
        (folder, rpc, event). Features will be a set of aggregation functions
        computed over the event index, thus returning a dataframe of shape
        ((n_folders, n_rpcs), n_features).

        The computed features in this implementation are:
        - efficiency: the number of detected events over the total
        - efficiency_error: the error on the computed efficiency
        - streamer_probability: the number of streamer events over the total
        - streamer_probability_error: the error on the computed streamer probability

        Parameters
        ----------
        data : pd.DataFrame
            the dataframe in the shape ((n_folders, n_rpcs, n_events), n_features)

        Returns
        -------
        pd.DataFrame
            a dataframe of shape ((n_folders, n_rpcs), n_features)
        """
        df = data
        grouped = df.groupby(["folder_num", "rpc"])
        detected = df.query("is_detected")
        grouped_detected = detected.groupby(["folder_num", "rpc"])
        events = df.reset_index(level="event").event
        n_events = events.count()
        efficiency = grouped.is_detected.agg(lambda x: x.sum() / x.count())
        efficiency_error = np.sqrt(efficiency * (1 - efficiency) / n_events)
        streamer_probability = grouped_detected.event_type.agg(
            lambda x: (x == olefin.SignalTypes.streamer).sum() / x.count()
        )
        streamer_probability_error = np.sqrt(
            streamer_probability * (1 - streamer_probability) / n_events
        )
        extra_charge_probability = grouped_detected.event_type.agg(
            lambda x: (x == olefin.SignalTypes.extra_charge).sum() / x.count()
        )
        extra_charge_probability_error = np.sqrt(
            extra_charge_probability * (1 - extra_charge_probability) / n_events
        )
        avalanche_mean = (
            detected.query("event_type == @olefin.SignalTypes.avalanche")
            .groupby(["folder_num", "rpc"])
            .event_charge.mean()
        )
        extra_charge_mean = (
            detected.query("event_type == @olefin.SignalTypes.extra_charge")
            .groupby(["folder_num", "rpc"])
            .event_charge.mean()
        )
        streamer_mean = (
            detected.query("event_type == @olefin.SignalTypes.streamer")
            .groupby(["folder_num", "rpc"])
            .event_charge.mean()
        )
        prompt_charge_mean = grouped_detected.event_charge.mean()

        return pd.DataFrame(
            {
                "efficiency": efficiency,
                "efficiency_error": efficiency_error,
                "streamer_probability": streamer_probability,
                "streamer_probability_error": streamer_probability_error,
                "extra_charge_probability": extra_charge_probability,
                "extra_charge_probability_error": extra_charge_probability_error,
                "avalanche": avalanche_mean,
                "extra_charge": extra_charge_mean,
                "streamer": streamer_mean,
                "prompt_charge": prompt_charge_mean,
            }
        )

    def run(self):
        """
        Run the analysis on the class

        This will call the calc_features function and sets the .data attribute.
        """
        self.data = self.calc_features(self.indata)
        return self.data


class EPDTAcquisitionAnalysis(BaseAnalysis):
    """Analysis at acquisition Level.

    An acquisition is the output of the wavedump program. Usually the
    acquisition level corresponds with a fixed HV point. A wavedump program
    can write txt files in the file system named wave<n>.txt where ``n`` is the
    number of the channel connected to the digitizer. An acquisition class
    takes a raw_data as input in the form of a pandas DataFrame of shape
    ((n_folders,n_rpc,n_events), n_features) with a mult index (folder, rpc, event)
    and compute acquisition relatd features, i.e. features depending on a single
    wavedump acquisition (such as efficiency or streamer probability for example).

    Attributes
    ----------
    data : pd.DataFrame
        the dataframe used by the class to run the analysis
    config : olefin.config
        the global config object
    """

    data = None
    config = None

    def __init__(self, config: "olefin.config", raw_data):
        self.config = config
        self.indata = raw_data

    @classmethod
    def from_folder(
        cls,
        config: "olefin.config",
        folder: t.Union[str, Path],
        event_class=olefin.EPDTEventAnalysis,
        signal_class=olefin.EPDTSignalAnalysis,
    ):
        """
        Create an acquisition analysis from a wavedump output folder.

        The folder is the result of a wavedump acquisition, containing wave<n>.txt
        files. The class allows to pass custom classes for signal and event analysis.
        Also, the class will store the event analysis on the attribute .event
        so that it can be easily accessed for inspecting event data
        """

        # Merge and update config from base configs
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        logger.debug(f"Creating Acquistion class from folder: {folder}")
        event_analysis = event_class.from_folder(parsed_config, folder, signal_class)
        event_analysis.run()
        logger.debug(f"Created Event from Acquisition factory: {folder}")
        acquisition_analysis = cls(parsed_config, event_analysis.data)
        acquisition_analysis.event = event_analysis
        return acquisition_analysis

    def calc_features(self, data) -> pd.DataFrame:
        """
        Calculate acquisition features.

        ``data`` should be a pandas DataFrame with shape
        ((n_folders, n_rpcs, n_events), n_features) with a multi index
        (folder, rpc, event). Features will be a set of aggregation functions
        computed over the event index, thus returning a dataframe of shape
        ((n_folders, n_rpcs), n_features).

        The computed features in this implementation are:
        - efficiency: the number of detected events over the total
        - efficiency_error: the error on the computed efficiency
        - streamer_probability: the number of streamer events over the total
        - streamer_probability_error: the error on the computed streamer probability

        Parameters
        ----------
        data : pd.DataFrame
            the dataframe in the shape ((n_folders, n_rpcs, n_events), n_features)

        Returns
        -------
        pd.DataFrame
            a dataframe of shape ((n_folders, n_rpcs), n_features)
        """
        df = data.reset_index(level="nevent")
        grouped = df.groupby(["folder_num", "rpc"])
        detected = df.query("is_detected")
        grouped_detected = detected.groupby(["folder_num", "rpc"])
        n_events = grouped.nevent.count()
        efficiency = grouped.is_detected.agg(lambda x: x.sum() / x.count()).astype(
            float
        )
        efficiency_error = np.sqrt(efficiency * (1 - efficiency) / n_events)

        streamer_probability = grouped.apply(
            lambda x: (
                x.is_detected & (x.event_type == olefin.SignalTypes.streamer)
            ).sum()
            / x.is_detected.sum()
        )
        streamer_probability_error = np.sqrt(
            streamer_probability * (1 - streamer_probability) / n_events
        )

        avalanche_events = detected.query(
            "event_type == @olefin.SignalTypes.avalanche"
        ).groupby(["folder_num", "rpc"])
        avalanche_mean = avalanche_events.event_charge.mean()
        avalanche_error = (
            avalanche_events.event_charge.std() / np.sqrt(len(detected))
            if len(detected)
            else 0
        )

        streamer_events = detected.query(
            "event_type == @olefin.SignalTypes.streamer"
        ).groupby(["folder_num", "rpc"])
        streamer_mean = streamer_events.event_charge.mean()
        streamer_error = (
            streamer_events.event_charge.std() / np.sqrt(len(detected))
            if len(detected)
            else 0
        )

        prompt_charge_mean = grouped_detected.event_charge.mean()
        prompt_charge_error = (
            grouped_detected.event_charge.std() / np.sqrt(len(detected))
            if len(detected)
            else 0
        )

        time_res_data = grouped_detected.time_peak.apply(
            self.calc_res_time,
            time_resolution=self.config.digitizer.time_resolution,
            bins=self.config.acquisition.time_resolution_bins,
            initial_params=self.config.acquisition.time_resolution_initial_params,
        )
        time_res = (
            time_res_data.loc[:, :, "time_res"]
            if len(time_res_data)
            else pd.Series([], name="time_res")
        )
        time_res_error = (
            time_res_data.loc[:, :, "time_res_error"]
            if len(time_res_data)
            else pd.Series([], name="time_res_error")
        )
        time_res_chi2 = (
            time_res_data.loc[:, :, "time_res_chi2"]
            if len(time_res_data)
            else pd.Series([], name="time_res_chi2")
        )

        cluster_size = grouped_detected.cluster_size.mean()
        cluster_size_error = grouped_detected.cluster_size.std() / np.sqrt(n_events)

        cluster_size_avalanche = avalanche_events.cluster_size.mean()
        cluster_size_avalanche_error = (
            avalanche_events.cluster_size.std() / np.sqrt(len(avalanche_events))
            if len(avalanche_events)
            else 0
        )

        cluster_size_streamer = avalanche_events.cluster_size.mean()
        cluster_size_streamer_error = (
            avalanche_events.cluster_size.std() / np.sqrt(len(avalanche_events))
            if len(streamer_events)
            else 0
        )

        time_over_threshold = grouped_detected.time_over_threshold.mean()
        time_over_threshold_error = (
            grouped_detected.time_over_threshold.std() / np.sqrt(n_events)
        )

        height = grouped_detected.event_height.mean()
        height_error = grouped_detected.event_height.std() / np.sqrt(n_events)

        return pd.DataFrame(
            {
                "efficiency": efficiency,
                "efficiency_error": efficiency_error,
                "streamer_probability": streamer_probability,
                "streamer_probability_error": streamer_probability_error,
                "avalanche": avalanche_mean,
                "avalanche_error": avalanche_error,
                "streamer": streamer_mean,
                "streamer_error": streamer_error,
                "prompt_charge": prompt_charge_mean,
                "prompt_charge_error": prompt_charge_error,
                "cluster_size": cluster_size,
                "cluster_size_error": cluster_size_error,
                "cluster_size_avalanche": cluster_size_avalanche,
                "cluster_size_avalanche_error": cluster_size_avalanche_error,
                "cluster_size_streamer": cluster_size_streamer,
                "cluster_size_streamer_error": cluster_size_streamer_error,
                "time_res": time_res,
                "time_res_error": time_res_error,
                "time_res_chi2": time_res_chi2,
                "height": height,
                "height_error": height_error,
                "time_over_threshold": time_over_threshold,
                "time_over_threshold_error": time_over_threshold_error,
            },
            # Force index to the one of the efficiency
            # because the other variables may be empty dataframes
            # and may loose the index
            index=efficiency.index,
        )

    @staticmethod
    def calc_res_time(
        times: np.ndarray,
        time_resolution: float,
        bins: t.Union[str, np.ndarray, int] = "auto",
        model: t.Callable = olefin.analysis.utils.gaussian_model_2,
        initial_params: t.Union[str, t.Tuple] = "auto",
    ) -> t.Dict:
        """Compute the full width half maximum distribution
        of a given set of times.

        The time_res is defined as sigma * sqrt(2 * log(2))

        Parameters
        ----------
        times: np.ndarray
            a 1d array of times

        Returns
        -------
        dict a dict containing the res_time and the chi squared resulted
            from the fit. res_time is NaN if all the values in times are NaN
        """
        if np.isnan(times).all() or len(times) < 2:
            return {
                "time_res": np.nan,
                "time_res_error": np.nan,
                "time_res_chi2": np.nan,
            }

        # Get the time_res time by fitting the time distribution
        # with a gaussian
        # Prepare the data
        counts, edges = np.histogram(times, bins=bins)
        edges = edges[:-1]  # edges are len(counts) + 1
        if initial_params == "auto":
            initial_params = (edges[counts.argmax()], 1, 2)
            bounds = ((-np.inf, 0, 0), (np.inf, 1e10, 1e10))

        params, params_error, chi2_reduced = olefin.analysis.utils.fit_model(
            x=edges, y=counts, model=model, initial_params=initial_params, bounds=bounds
        )

        time_res = params[2] * time_resolution
        time_res_error = params_error[2] * time_resolution

        return {
            "time_res": time_res,
            "time_res_error": time_res_error,
            "time_res_chi2": chi2_reduced,
        }

    def run(self):
        """
        Run the analysis on the class

        This will call the calc_features function and sets the .data attribute.
        """
        self.data = self.calc_features(self.indata)
        self.data = AcquisitionAnalysisDataFrame(self.data)
        return self.data


class AcquisitionAnalysisDataFrame(pd.DataFrame):
    """Extension of pandas DataFrame class for custom plotting"""

    def olefin_plot(
        self,
        overlap="rpc",
        features=None,
        add_fit=True,
        dfrun=None,
        feature_subplot_kwargs=None,
    ):
        """Plot the desired feature over voltage.

        This method allows to choose which feature to plot and how to
        group them together. It also allows to pass a 'overlap' that could be
        either 'rpc' or 'run_name' and allows to plot multiple dataset of
        that same overlap value in the same subplot."""
        index_groups = ["rpc", "run_name"]
        non_overlap = list(set(index_groups) - set([overlap]))[0]
        features = features or [
            ["efficiency", "streamer_probability"],
            ["avalanche", "streamer"],
            "cluster_size",
            "time_res",
        ]
        feature_subplot_kwargs = feature_subplot_kwargs or {
            "efficiency": {"ylim": (0, 1)},
            "avalanche": {"ylabel": "Charge [pC]"},
            "cluster_size": {"ylabel": "Cluster size [#strips*2cm]"},
            "time_res": {"ylabel": "Time [ns]"},
        }
        colors = mpl.cm.tab10.colors
        markers = list(mpl.lines.Line2D.markers.keys())[2:]
        unique_non_overlap_values = (
            self.index.get_level_values(non_overlap).unique().tolist()
        )
        unique_overlap_values = self.index.get_level_values(overlap).unique().tolist()
        ncols = len(unique_non_overlap_values)
        nrows = len(features)
        fig, axs = plt.subplots(nrows, ncols, figsize=(ncols * 4, nrows * 4))
        if ncols == 1:
            axs = axs[:, np.newaxis]
        for ix_feature_group, feature_group in enumerate(features):
            if isinstance(feature_group, str):
                feature_group = [feature_group]
            for ncol, item in enumerate(unique_non_overlap_values):
                ax = axs[ix_feature_group, ncol]
                ax.set_title(f"{non_overlap} {item}")
                for noverlap, overlap_value in enumerate(unique_overlap_values):
                    for ix_feature, feature in enumerate(feature_group):
                        subset = (
                            self.xs(item, level=non_overlap)
                            .xs(overlap_value, level=overlap)
                            .get(feature)
                        )
                        xvalues = subset.index.get_level_values("voltage")
                        yvalues = subset.values
                        yvalues_error = subset.get(f"{feature}_error", [])
                        if overlap == "run_name":
                            label = f"{overlap_value} {feature}"
                        else:
                            label = f"{overlap} {overlap_value} {feature}"
                        linestyle = "" if feature == "efficiency" and add_fit else "--"
                        if len(yvalues_error):
                            ax.errorbar(
                                xvalues,
                                yvalues,
                                yvalues_error,
                                linestyle=linestyle,
                                label=label,
                                color=colors[noverlap],
                                marker=markers[ix_feature],
                            )
                        else:
                            ax.plot(
                                xvalues,
                                yvalues,
                                linestyle=linestyle,
                                label=label,
                                color=colors[noverlap],
                                marker=markers[ix_feature],
                            )

                        if feature == "efficiency" and add_fit:
                            rpc = overlap_value if overlap == "rpc" else item
                            fit_params = dfrun.loc[rpc][
                                ["effmax", "gamma", "hv50"]
                            ].tolist()
                            if any(fit_params):
                                xfit = np.arange(
                                    xvalues.min() - 200, xvalues.max() + 200, 50
                                )
                                yfit = olefin.analysis.implementations.run.RunAnalysis.efficiency_model(
                                    xfit, *fit_params
                                )
                                ax.plot(
                                    xfit, yfit, "-", label="", color=colors[noverlap]
                                )
                        ax.set_title(f"{non_overlap} {item}")
                        ax.set(**feature_subplot_kwargs.get(feature, {}))
                        ax.legend(loc="upper left", bbox_to_anchor=(1, 1))

        return fig, axs


class RateAcquisitionAnalysis(BaseAnalysis):
    """Analysis at acquisition Level.

    TODO: write

    Attributes
    ----------
    data : pd.DataFrame
        the dataframe used by the class to run the analysis
    config : olefin.config
        the global config object
    """

    data = None
    config = None

    def __init__(self, config: "olefin.config", raw_data):
        self.config = config
        self.indata = raw_data

    @classmethod
    def from_folder(
        cls,
        config: "olefin.config",
        folder: t.Union[str, Path],
        signal_class=olefin.RateSignalAnalysis,
    ):
        """
        Create an acquisition analysis from a wavedump output folder.

        The folder is the result of a wavedump acquisition, containing wave<n>.txt
        files. The class allows to pass custom classes for signal and event analysis.
        Also, the class will store the event analysis on the attribute .event
        so that it can be easily accessed for inspecting event data
        """
        res = []
        signal_analyses = []
        folder = Path(folder)
        voltage = int(folder.stem)

        # Merge and update config from base configs
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        for wave_name, values in parsed_config.acquisition.strip_map.items():
            wavefile = folder / f"{wave_name}.txt"
            wave_num = int(wave_name.strip("wave"))
            rpc, strip = values["rpc"], values["strip"]
            if not wavefile.exists():
                logger.warning(
                    f"{wave_name} present in strip_map but not found in folder {folder}"
                )
            signal_analysis = signal_class.from_wavefile(parsed_config, wavefile)
            df = signal_analysis.run()
            dfagg = df.agg("mean")
            dfagg["voltage"] = voltage
            dfagg["wave_num"] = wave_num
            dfagg["rpc"] = rpc
            dfagg["strip"] = strip
            signal_analyses.append(signal_analysis)
            res.append(dfagg)
        df = pd.DataFrame(res).sort_values(by=["rpc", "strip"])
        try:
            df["rpc"] = df.rpc.astype(int)
        except Exception:
            logging.debug("Could not parse rpc name as integer")
        df["strip"] = df.strip.astype(int)
        df = df.pivot_table(index=["voltage", "rpc"], columns="strip")
        # Rename columns so that every level has a name
        df.columns.names = ["feature", "strip"]
        acquisition_analysis = cls(parsed_config, df)
        acquisition_analysis.signals = signal_analyses
        return acquisition_analysis

    def calc_features(self, data) -> pd.DataFrame:
        """
        Calculate acquisition features.

        TODO: write

        Parameters
        ----------
        data : pd.DataFrame
            the dataframe in the shape ((n_folders, n_rpcs, n_events), n_features)

        Returns
        -------
        pd.DataFrame
            a dataframe of shape ((n_folders, n_rpcs), n_features)
        """
        stacked = data.stack()
        df = stacked.groupby(["voltage", "rpc"])
        # mean_count = df["count"].mean()
        # mean_rate = df.rate.mean()
        # mean_count_err = np.sqrt(np.sum(df["count"].apply(np.square)))
        # mean_rate_err = np.sqrt(np.sum(df["rate"].apply(np.square)))

        return df.agg(
            {
                "count": lambda x: x.mean(),
                "count_error": lambda x: np.sqrt(np.sum(x**2)),
                "rate": lambda x: x.mean(),
                "rate_error": lambda x: np.sqrt(np.sum(x**2)),
            }
        )

    def run(self):
        """
        Run the analysis on the class

        This will call the calc_features function and sets the .data attribute.
        """
        self.data = self.calc_features(self.indata)
        return self.data
