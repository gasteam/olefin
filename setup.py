import setuptools
from setuptools.extension import Extension
import logging
import os


# FIXME it errors when reading the README.md file
# with open("README.md", "r") as fh:
#     long_description = fh.read()


setuptools.setup(
    name="olefin",
    version="1.0.11",
    author="Gianluca Rigoletti",
    description="A framework for analysing RPC waveform data for EP-DT Gas System group.",
    long_description="check README.md",  # FIXME put here the readme contents
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    extras_require={"root-numpy": "root-numpy"},
    install_requires=[
        "matplotlib>=3.5.0; python_version<'3.8'",
        "matplotlib>=3.6.3; python_version>='3.8'",
        "numba>=0.56.4",
        "numpy>=1.21.0; python_version<'3.12'",
        "numpy>=1.26.0; python_version>='3.12'",
        "openpyxl>=3.1.0",
        "pandas>=1.3.0; python_version<'3.8'",
        "pandas>=2.0.0; python_version>='3.8'",
        "psycopg2-binary>=2.9.6",
        "python-box>=7.0.0",
        "pyyaml>=5.0.0",
        "scipy>=1.7.0; python_version<'3.8'",
        "scipy>=1.10.0; python_version>='3.8'",
        "tqdm>=4.0.0",
    ],
)
